#include <sys/types.h>
#include <ctype.h>
#include <math.h>
#include <stdio.h>
#include <string.h>
#include <time.h>
#include "mud.h"

void do_sigattacka( CHAR_DATA * ch, char *argument ) 
{
  
if( IS_NPC( ch ) )
  {
    send_to_char( "Mobs cannot set a signature attack.\n\r", ch );
    return;
  }

if( !ch->desc )
  {
    bug( "do_signatureattacka: no descriptor", 0 );
    return;
  }  

if( xIS_SET( ch->act, PLR_CAN_SIGATTACK ) )
  {
    send_to_char( "You already have your attack authorized. Type '&Rsignaturedeauth yes&D' to remove the authorization. This will allow you to edit your attack.", ch );
    return;
  }      
  
if( xIS_SET( ( ch )->in_room->room_flags, ROOM_GRAV ) )
  {
    ch_printf( ch, "You can't do that in a grav room.\n\r" );
    return;
  }
  
  switch ( ch->substate )
  {
    default:
        /*
         * bug( "do_sigattacka: illegal substate", 0 );
         */ 
    return;

    case SUB_RESTRICTED:
    send_to_char( "You cannot use this command from within another command.\n\r", ch );
    return;
    case SUB_NONE:
    do_help( ch, "sigattack" );
    send_to_char( "&RPlease read the above guidelines before writing your signature attack.&w\n\r", ch );
    send_to_char( "&GTHIS PORTION OF THE ATTACK IS WHAT YOU SEE WHEN YOU FIRE THE ATTACK.&w\n\r", ch );
    if( xIS_SET( ch->act, PLR_CAN_SIGATTACK ) )
    xREMOVE_BIT( ch->act, PLR_CAN_SIGATTACK );
    ch->substate = SUB_PERSONAL_SIGATTACKA;
    ch->dest_buf = ch;
    start_editing( ch, ch->pcdata->sigattacka );
    return;
    case SUB_PERSONAL_SIGATTACKA:
    STRFREE( ch->pcdata->sigattacka );
    ch->pcdata->sigattacka = copy_buffer( ch );
    stop_editing( ch );
    send_to_char( "\n\r&wIf you would like your attack to be queued for authorization, please\n\r"
                  "type '&WREQATTACKAUTH&w'.  Please do not ask for an admin to look over your attack.\n\r", ch );
    return;
  }
}

void do_sigattackb( CHAR_DATA * ch, char *argument ) 
{
  
if( IS_NPC( ch ) )
  {
    send_to_char( "Mobs cannot set a signature attack.\n\r", ch );
    return;
  }

if( !ch->desc )
  {
    bug( "do_signatureattackb: no descriptor", 0 );
    return;
  }  

if( xIS_SET( ch->act, PLR_CAN_SIGATTACK ) )
  {
    send_to_char( "You already have your attack authorized. Type '&Rsignaturedeauth yes&D' to remove the authorization. This will allow you to edit your attack.", ch );
    return;
  }      
  
if( xIS_SET( ( ch )->in_room->room_flags, ROOM_GRAV ) )
  {
    ch_printf( ch, "You can't do that in a grav room.\n\r" );
    return;
  }
  
  switch ( ch->substate )
  {
    default:
        /*
         * bug( "do_sigattack1: illegal substate", 0 );
         */ 
    return;

    case SUB_RESTRICTED:
    send_to_char( "You cannot use this command from within another command.\n\r", ch );
    return;
    case SUB_NONE:
    do_help( ch, "sigattack" );
    send_to_char( "&RPlease read the above guidelines before writing your signature attack.&w\n\r", ch );
    send_to_char( "&GTHIS PORTION OF THE ATTACK IS WHAT YOUR OPPONENT SEES WHEN YOU FIRE THE ATTACK.&w\n\r", ch );
    if( xIS_SET( ch->act, PLR_CAN_SIGATTACK ) )
    xREMOVE_BIT( ch->act, PLR_CAN_SIGATTACK );
    ch->substate = SUB_PERSONAL_SIGATTACKB;
    ch->dest_buf = ch;
    start_editing( ch, ch->pcdata->sigattackb );
    return;
    case SUB_PERSONAL_SIGATTACKB:
    STRFREE( ch->pcdata->sigattackb );
    ch->pcdata->sigattackb = copy_buffer( ch );
    stop_editing( ch );
    send_to_char( "\n\r&wIf you would like your attack to be queued for authorization, please\n\r"
                  "type '&WREQATTACKAUTH&w'.  Please do not ask for an admin to look over your attack.\n\r", ch );
    return;
  }

}

void do_sigattackc( CHAR_DATA * ch, char *argument ) 
{
  
if( IS_NPC( ch ) )
  {
    send_to_char( "Mobs cannot set a signature attack.\n\r", ch );
    return;
  }

if( !ch->desc )
  {
    bug( "do_signatureattackc: no descriptor", 0 );
    return;
  }  

if( xIS_SET( ch->act, PLR_CAN_SIGATTACK ) )
  {
    send_to_char( "You already have your attack authorized. Type '&Rsignaturedeauth yes&D' to remove the authorization. This will allow you to edit your attack.", ch );
    return;
  }      
  
if( xIS_SET( ( ch )->in_room->room_flags, ROOM_GRAV ) )
  {
    ch_printf( ch, "You can't do that in a grav room.\n\r" );
    return;
  }
  
  switch ( ch->substate )
  {
    default:
        /*
         * bug( "do_sigattackc: illegal substate", 0 );
         */ 
    return;

    case SUB_RESTRICTED:
    send_to_char( "You cannot use this command from within another command.\n\r", ch );
    return;
    case SUB_NONE:
    do_help( ch, "sigattack" );
    send_to_char( "&RPlease read the above guidelines before writing your signature attack.&w\n\r", ch );
    send_to_char( "&GTHIS PORTION OF THE ATTACK IS WHAT EVERYONE OTHER THAN YOU OR YOUR OPPONENT SEES WHEN YOU FIRE THE ATTACK.&w\n\r", ch );
    if( xIS_SET( ch->act, PLR_CAN_SIGATTACK ) )
    xREMOVE_BIT( ch->act, PLR_CAN_SIGATTACK );
    ch->substate = SUB_PERSONAL_SIGATTACKC;
    ch->dest_buf = ch;
    start_editing( ch, ch->pcdata->sigattackc );
    return;
    case SUB_PERSONAL_SIGATTACKC:
    STRFREE( ch->pcdata->sigattackc );
    ch->pcdata->sigattackc = copy_buffer( ch );
    stop_editing( ch );
    send_to_char( "\n\r&wIf you would like your attack to be queued for authorization, please\n\r"
                  "type '&WREQATTACKAUTH&w'.  Please do not ask for an admin to look over your attack.\n\r", ch );
    return;
  }

}

void do_setattack( CHAR_DATA *ch, char *argument )
{
    char arg1[MAX_INPUT_LENGTH];
    char arg2[MAX_INPUT_LENGTH];

    set_char_color( AT_PLAIN, ch );
    if ( IS_NPC( ch ) )
    {
	send_to_char( "Huh?\n\r", ch );
	return;
    }

    if( xIS_SET( ch->act, PLR_CAN_SIGATTACK ) )
    {
       send_to_char( "You already have your attack authorized. Type '&Rsignaturedeauth yes&D' to remove the authorization. This will allow you to edit your attack.", ch );
       return;
    }      

    argument = one_argument( argument, arg1 );
    argument = one_argument( argument, arg2 );
    if ( arg1[0] == '\0' || arg2[0] == '\0' )
    {
	send_to_char( "Usage: setattack <field> <#>\n\r", ch );
	send_to_char( "\n\rField being one of:\n\r", ch );
	send_to_char( " mindamage maxdamage energy focus\n\r", ch );
	return;
    }

	if (!str_cmp(arg1, "mindamage"))
	{
	char buf[MAX_STRING_LENGTH];
	int value;

	value = atoi( arg2 );    
  	if( value > 50 )
	{
       send_to_char( "Minimum damage can't be more than 50.\n\r", ch );
       value = 50;
    }    
	ch->pcdata->siglow = value;
	sprintf(buf, "Setting minimum damage for signature attack to %d.\n\r", value );
	send_to_char( buf, ch );
		if( ch->pcdata->sighigh < ch->pcdata->siglow )
		{
	      send_to_char( "&RWARNING: Maximum signature attack damage is lower than minimum.\n\rRaising maximum damage to equal minimum damage.\n\r", ch);
	      ch->pcdata->sighigh = ch->pcdata->siglow;
	    }    
	save_char_obj( ch );
    return;
    }

    if ( !str_cmp( arg1, "maxdamage" ) )
    {
	char buf[MAX_STRING_LENGTH];
    int value;

  	value = atoi( arg2 );    
  	if( value > 75 )
	{
       send_to_char( "Max damage can't be more than 75.\n\r", ch );
       value = 75;
    }
	ch->pcdata->sighigh = value;
	sprintf(buf, "Setting maximum damage for signature attack to %d.\n\r", value );
	send_to_char( buf, ch );
		if( ch->pcdata->siglow > ch->pcdata->sighigh )
		{
	      send_to_char( "&RWARNING: Maximum signature attack damage is lower than minimum.\n\rRaising maximum damage to equal minimum damage.\n\r", ch);
	      ch->pcdata->sighigh = ch->pcdata->siglow;
	    }    
	save_char_obj( ch );
	return;
    }

    if ( !str_cmp( arg1, "energy" ) )
    {
	char buf[MAX_STRING_LENGTH];
    int value;

	value = atoi( arg2 );        
	if( value < 500 )
	{
       send_to_char( "Energy can't be less than 500.\n\r", ch );
	   value = 500;
    }
    ch->pcdata->sigmana = value;
	sprintf(buf, "Setting energy cost for signature attack to %d.\n\r", value );
	send_to_char( buf, ch );
	save_char_obj( ch );
	return;
    }

    if ( !str_cmp( arg1, "focus" ) )
    {
	char buf[MAX_STRING_LENGTH];
    int value;

	value = atoi( arg2 );    
	if(value < 15)
	{
       send_to_char( "Focus can't be less than 15.\n\r", ch );
	   value = 15;
    }
	ch->pcdata->sigfocus = value;
	sprintf(buf, "Setting focus for signature attack to %d.\n\r", value );
	send_to_char( buf, ch );
	save_char_obj( ch );
	return;
    }
}

void do_sigdeauth( CHAR_DATA * ch, char *argument ) 
{
  char arg[MAX_INPUT_LENGTH];
  char buf[MAX_INPUT_LENGTH];
  argument = one_argument( argument, arg );

  if( !xIS_SET( ch->act, PLR_CAN_SIGATTACK ) )
  {
    send_to_char( "Your signature attack isn't authorized. There is no need for you to remove an\n\r authorization you don't have.", ch);
    return;
  }    

  if( xIS_SET( ch->act, PLR_CAN_SIGATTACK ) && strcmp( arg, "yes" ) )
  {
    send_to_char( "Syntax: signaturedeauth yes", ch);
    send_to_char( "Remember, once you deauth your attack, you must get it re-authorized by the admins.", ch);
    return;
  }    

  if( !strcmp( arg, "yes" ) )
  {
     if( xIS_SET( ch->act, PLR_CAN_SIGATTACK ) )
     xREMOVE_BIT( ch->act, PLR_CAN_SIGATTACK );
     sprintf( buf, "%s's signature attack is no longer authorized", ch->name);
     do_ainfo( ch, buf );
     send_to_char( "Your signature attack is no longer authorized. You may now make changes to your\n\r attack settings.\n\r", ch );
  }  
  else
  {
      send_to_char( "syntax: signaturedeauth yes\n\r", ch);
      send_to_char( "Remember, once you deauth your attack, you must get it re-authorized by the admins.", ch);
  }
}

void do_sigsettings( CHAR_DATA * ch, char *argument )
{
	char buf1[MAX_STRING_LENGTH];
	char buf2[MAX_STRING_LENGTH];
	char buf3[MAX_STRING_LENGTH];
	char buf4[MAX_STRING_LENGTH];
	char buf5[MAX_STRING_LENGTH];
	char buf6[MAX_STRING_LENGTH];
	char buf7[MAX_STRING_LENGTH];
	char buf8[MAX_STRING_LENGTH];
	char buf9[MAX_STRING_LENGTH];
	char buf10[MAX_STRING_LENGTH];

	int ldcost;
	int hdcost;
	int fcost;
	int mcost;
	int rppcost;
	int rppreq;
	
	set_char_color(AT_GREY, ch);

	send_to_char_color( "Signature Attack Settings:\n\r", ch);
    sprintf(buf1, "&DDisplay to you:\n\r%s", ch->pcdata->sigattacka);
    sprintf(buf2, "&DDisplay to opponent:\n\r%s", ch->pcdata->sigattackb);
    sprintf(buf3, "&DDisplay to onlookers:\n\r%s", ch->pcdata->sigattackc);
    send_to_char_color( buf1, ch );
    send_to_char_color( buf2, ch );
    send_to_char_color( buf3, ch );
    sprintf(buf4, "&DMinimum Damage: %d\n\r", ch->pcdata->siglow);
    sprintf(buf5, "&DMaximum Damage: %d\n\r", ch->pcdata->sighigh);
    sprintf(buf6, "&DEnergy Cost   : %d\n\r", ch->pcdata->sigmana);    
    sprintf(buf7, "&DFocus         : %d\n\r", ch->pcdata->sigfocus);    
    send_to_char_color( buf4, ch );
    send_to_char_color( buf5, ch );
    send_to_char_color( buf6, ch );
    send_to_char_color( buf7, ch );
    if( !xIS_SET( ch->act, PLR_CAN_SIGATTACK ) )
    {
	ldcost = ((ch->pcdata->siglow - 15)/3) *5;
	hdcost = ((ch->pcdata->sighigh - 18)/6) *5;
	fcost = (40 - ch->pcdata->sigfocus) *5;
	mcost = ((5000 - ch->pcdata->sigmana)/500) *5;

	rppcost = ldcost + hdcost + fcost + mcost;
	rppreq = rppcost - (int)ch->pcdata->quest_curr;
	if( rppreq < 0 );
       rppreq = 0;
	if( rppcost < 40 )
	   rppcost = 40;
	ch->pcdata->rppcost = rppcost;
    sprintf(buf8, "&DRPP Cost      : %d&W\n\r", ch->pcdata->rppcost);
    sprintf(buf9, "&DCurrent RPP   : %d&W\n\r", ch->pcdata->quest_curr);
    sprintf(buf10, "&DRPP Required for Authorization: &R%d\n\r", rppreq);
    send_to_char( buf8, ch );
    send_to_char( buf9, ch );
send_to_char( buf10, ch );
    return;
    }      
    else
    {
    send_to_char( "You already have your attack authorized. Type '&Rsignaturedeauth yes&D' to remove the authorization. This will allow you to edit your attack.", ch );
    return;
    }    
}

void do_viewsig( CHAR_DATA * ch, char *argument )
{
	char buf1[MAX_STRING_LENGTH];
	char buf2[MAX_STRING_LENGTH];
	char buf3[MAX_STRING_LENGTH];
	char buf4[MAX_STRING_LENGTH];
	char buf5[MAX_STRING_LENGTH];
	char buf6[MAX_STRING_LENGTH];
	char buf7[MAX_STRING_LENGTH];
	char buf8[MAX_STRING_LENGTH];
	char buf9[MAX_STRING_LENGTH];
	char buf10[MAX_STRING_LENGTH];
	char arg[MAX_STRING_LENGTH];
    CHAR_DATA *victim;
   	int ldcost;
	int hdcost;
	int fcost;
	int mcost;
	int rppcost;
	int rppreq;
	
    argument = one_argument( argument, arg );
    victim = NULL;
	
    if( arg[0] == '\0' )
    {
      send_to_char( "View whose signature attack info?\n\r", ch );
      return;
    }

    if( ( victim = get_char_world( ch, arg ) ) == NULL )
    {
      send_to_char( "They aren't here.\n\r", ch );
      return;
    }

    if( IS_NPC( victim ) )
    {
      send_to_char( "Not on NPC's.\n\r", ch );
      return;
    }

	set_char_color(AT_GREY, ch);

	send_to_char_color( "Signature Attack Settings:\n\r", ch);
    sprintf(buf1, "&DTO_CHAR:\n\r%s", victim->pcdata->sigattacka);
    sprintf(buf2, "&DTO_VICT:\n\r%s", victim->pcdata->sigattackb);
    sprintf(buf3, "&DTO_NOTVICT:\n\r%s", victim->pcdata->sigattackc);
    send_to_char_color( buf1, ch );
    send_to_char_color( buf2, ch );
    send_to_char_color( buf3, ch );
    sprintf(buf4, "&DMinimum Damage: %d\n\r", victim->pcdata->siglow);
    sprintf(buf5, "&DMaximum Damage: %d\n\r", victim->pcdata->sighigh);
    sprintf(buf6, "&DEnergy Cost   : %d\n\r", victim->pcdata->sigmana);    
    sprintf(buf7, "&DFocus         : %d\n\r", victim->pcdata->sigfocus);    
    send_to_char_color( buf4, ch );
    send_to_char_color( buf5, ch );
    send_to_char_color( buf6, ch );
    send_to_char_color( buf7, ch );
    if( !xIS_SET( victim->act, PLR_CAN_SIGATTACK ) )
    {
   	ldcost = ((victim->pcdata->siglow - 15)/3) *5;
	hdcost = ((victim->pcdata->sighigh - 18)/6) *5;
	fcost = (40 - victim->pcdata->sigfocus) *5;
	mcost = ((5000 - victim->pcdata->sigmana)/500) *5;

	rppcost = ldcost + hdcost + fcost + mcost;
	rppreq = rppcost - victim->pcdata->quest_curr;
	if( rppreq < 0 );
       rppreq = 0;
	if( rppcost < 40 )
	   rppcost = 40;
	victim->pcdata->rppcost = rppcost;
    sprintf(buf8, "&DRPP Cost      : %d\n\r", victim->pcdata->rppcost);
    sprintf(buf9, "&DCurrent RPP   : %d&W\n\r", victim->pcdata->quest_curr);
    sprintf(buf10, "&DRPP Required for Authorization: &R%d\n\r", rppreq);
    send_to_char( buf8, ch );
    send_to_char( buf9, ch );
  send_to_char( buf10, ch );
    }      
    return;
}

void do_reqattack( CHAR_DATA * ch, char *argument ) 
{

if( IS_NPC( ch ) )
return;

if( xIS_SET( ch->act, PLR_CAN_SIGATTACK ) )
  {
    send_to_char( "Your attack has already been authorized.\n\r", ch );
    return;
  }

if( xIS_SET( ch->act, PLR_REQATTACK ) )
  {
    send_to_char( "Your attack has already been queued.\n\r", ch );
    return;
  }

xSET_BIT( ch->act, PLR_REQATTACK );
send_to_char( "Your signature attack has been queued for authorization.\n\rPlease wait until an administrator can authorize it.\n\r", ch );
return;
}


void do_signature_attack( CHAR_DATA * ch, char *argument )
{
  CHAR_DATA *victim;
  int dam = 0;

  if( IS_NPC( ch ) && is_split( ch ) )
  {
//    if( !ch->master )
      return;
/*    if( !can_use_skill( ch->master, number_percent(  ), gsn_signature_attack) )
      return;*/
//Perhaps making it so splits can do the attack. Right now I don't know how to do this. -Kilan
  }

  if( IS_NPC( ch ) && IS_AFFECTED( ch, AFF_CHARM ) )
  {
    send_to_char( "You can't concentrate enough for that.\n\r", ch );
    return;
  }

  if( !IS_NPC( ch ) && !xIS_SET( ch->act, PLR_CAN_SIGATTACK ) )
  {
    send_to_char( "You need to write a signature attack and have it authed before you can use it.\n\rSee &RHELP SIGATTACK&D for more information.", ch );
    return;
  }

  if( ( victim = who_fighting( ch ) ) == NULL )
  {
    send_to_char( "You aren't fighting anyone.\n\r", ch );
    return;
  }

  if( ch->mana < ch->pcdata->sigmana )
  {
    send_to_char( "You don't have enough energy.\n\r", ch );
    return;
  }
  if( ch->focus < ch->pcdata->sigfocus )
  {
    send_to_char( "You need to focus more.\n\r", ch );
    return;
  }
  else
    ch->focus -= ch->pcdata->sigfocus;

  WAIT_STATE( ch, 8 );

    if ( can_use_skill(ch, number_percent(),gsn_signature_attack ) )
    {
    dam = get_attmod( ch, victim ) * number_range( ch->pcdata->siglow, ch->pcdata->sighigh );
    if( ch->charge > 0 )
      dam = chargeDamMult( ch, dam );
    act( AT_SKILL, ch->pcdata->sigattacka, ch, num_punct(dam), victim, TO_CHAR );
    act( AT_SKILL, ch->pcdata->sigattackb, ch, num_punct(dam), victim, TO_VICT );
    act( AT_SKILL, ch->pcdata->sigattackc, ch, num_punct(dam), victim, TO_NOTVICT );

    dam = ki_absorb( victim, ch, dam, gsn_signature_attack );
    learn_from_success( ch, gsn_signature_attack );
    global_retcode = damage( ch, victim, dam, TYPE_HIT );
  }
  else
  {
    act( AT_SKILL, "You launch your specialized attack at $N, but it misses!", ch, NULL, victim, TO_CHAR );
    act( AT_SKILL, "$n launches $s specialized attack at you, but it misses!", ch, NULL, victim, TO_VICT );
    act( AT_SKILL, "$n launches $s specialized attack at $N, but it misses!", ch, NULL, victim, TO_NOTVICT );
    learn_from_failure( ch, gsn_signature_attack );
    global_retcode = damage( ch, victim, 0, TYPE_HIT );
  }
  ch->mana -= ch->pcdata->sigmana;
  return;
}


