/****************************************************************************
 *  __                                                                      *
 * |  ""--.--.._                                             __..    ,--.   *
 * |       `.   "-.'""\_...-----..._   ,--. .--..-----.._.""|   |   /   /   *
 * |_   _    \__   ).  \           _/_ |   \|  ||  ..    >  `.  |  /   /    *
 *   | | `.   ._)  /|\  \ .-"""":-"   "-.   `  ||  |.'  ,'`. |  |_/_  /     *
 *   | |_.'   |   / ""`  \  ===/  ..|..  \     ||      < ""  `.  "  |/__    *
 *   `.      .    \ ,--   \-..-\   /"\   /     ||  |>   )--   |    /    |   *
 *    |__..-'__||__\   |___\ __.:-.._..-'_|\___||____..-/  |__|--""____/    *
 *                                                                          *
 *                     New Universe: Hopes Beginning                        *
 *                     Coders: Vegeta, Cyris, Goku                          *
 *                Special Thanks Goes out to Remcon of LOP                  *
 ***************************************************************************/

#if defined(macintosh)
#include <types.h>
#else
#include <sys/types.h>
#include <sys/time.h>
#endif

#include <ctype.h>
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <unistd.h>
#include <stdarg.h>
#include "mud.h"

/* New Types/Look by Vegeta */
/*   
 * Globals
 */
char *current_date args ((void));
int   recent_changes	 args( ( void ) );
int   strip_change_day   args( ( char *date ) );
int   strip_change_month args( ( char *date ) );
int   strip_change_year  args( ( char *date ) );
int   recent_changes	 args( ( void ) );
int num_changes args( ( void ) );


void stc( const char *txt, CHAR_DATA *ch )
{
	send_to_char( txt, ch );
}

int col_str_len (char *txt)
{
   int pos, len;
   
   len = 0;
   for (pos = 0; txt[pos] != '\0'; pos++)
   {
      if (txt[pos] != '#')
      {
         len++;
         continue;
      }
      pos++;
      if (txt[pos] == '\0')
         return len;
      if (txt[pos] == '#' || txt[pos] == '-')
         len++;
   }
   
   return len;
}

void ctc (char *txt, CHAR_DATA * ch, int length)
{
   int len, pos;
   char buf[MAX_STRING_LENGTH];
    
   len = (length - col_str_len (txt)) / 2;
   for (pos = 0; pos < len; pos++)
   {
      buf[pos] = ' ';
   }
   buf[pos] = '\0';
   send_to_char (buf, ch);
   send_to_char (txt, ch);
}

void cent_to_char (char *txt, CHAR_DATA * ch)
{
   int len, pos;
   char buf[MAX_STRING_LENGTH];

   len = (80 - col_str_len (txt)) / 2;
   for (pos = 0; pos < len; pos++)
   {
      buf[pos] = ' ';
   }  
   buf[pos] = '\0';

   send_to_char (buf, ch);
   send_to_char (txt, ch);
   send_to_char ("\n\r", ch);
}
/*
 * Local Functions
 */

int maxChanges;
#define  NULLSTR( str )  ( str == NULL || str[0] == '\0' )
NEWCHANGE_DATA *changes_table;

char *nshort_date (time_t time)
{
   static char buf[20];
   char tmp[20];
   char *date;

   if (time < 0)
   {
      time = current_time;
   }

   date = ctime (&time);

   tmp[0] = date[4];
   tmp[1] = date[5];
   tmp[2] = date[6];
   tmp[3] = '\0';
   if (!str_cmp (tmp, "jan"))
   {
      buf[0] = '0';
      buf[1] = '1';
   }
   else if (!str_cmp (tmp, "feb"))
   {
      buf[0] = '0';
      buf[1] = '2';
   }
   else if (!str_cmp (tmp, "mar"))
   {
      buf[0] = '0';
      buf[1] = '3';
   }
   else if (!str_cmp (tmp, "apr"))
   {
      buf[0] = '0';
      buf[1] = '4';
   }
   else if (!str_cmp (tmp, "may"))
   {
      buf[0] = '0';
      buf[1] = '5';
   }
   else if (!str_cmp (tmp, "jun"))
   {
      buf[0] = '0';
      buf[1] = '6';
   }
   else if (!str_cmp (tmp, "jul"))
   {
      buf[0] = '0';
      buf[1] = '7';
   }
   else if (!str_cmp (tmp, "aug"))
   {
      buf[0] = '0';
      buf[1] = '8';
   }
   else if (!str_cmp (tmp, "sep"))
   {
      buf[0] = '0';
      buf[1] = '9';
   }
   else if (!str_cmp (tmp, "oct"))
   {
      buf[0] = '1';
      buf[1] = '0';
   }
   else if (!str_cmp (tmp, "nov"))
   {
      buf[0] = '1';
      buf[1] = '1';
   }
   else if (!str_cmp (tmp, "dec"))
   {
      buf[0] = '1';
      buf[1] = '2';
   }
   else
   {
      buf[3] = '9';
      buf[4] = '9';
   }

   buf[2] = '/';

   if (date[8] == ' ')
      buf[3] = '0';
   else
      buf[3] = date[8];

   buf[4] = date[9];

   buf[5] = '/';

   buf[6] = date[20];
   buf[7] = date[21];
   buf[8] = date[22];
   buf[9] = date[23];

   return buf;
}

void load_changes (void)
{
   FILE *fp;
   int i;

   if (!(fp = fopen (CHANGES_FILE, "r")))
   {
      bug ("Could not open Changes File for reading.", 0);
      return;
   }
   int fcheck;
   fcheck =fscanf (fp, "%d\n", &maxChanges);

   /* Use malloc so we can realloc later on */
   changes_table = malloc (sizeof (NEWCHANGE_DATA) * (maxChanges + 1));

   for (i = 0; i < maxChanges; i++)
   {
      changes_table[i].change = fread_string (fp);
      changes_table[i].coder = fread_string (fp);
      changes_table[i].date = fread_string (fp);
      changes_table[i].mudtime = fread_number (fp);
      changes_table[i].imm = fread_number (fp);
	  changes_table[i].type = fread_string (fp);
   }

   changes_table[maxChanges].coder = str_dup ("");
   fclose (fp);
   return;			/* just return */
}

char *current_date ()
{
   static char buf[128];
   struct tm *t;
   time_t nowtime;
   nowtime = time (&current_time);
   t = localtime (&nowtime);
   strftime (buf, 100, "%d-%b-%Y", t);
   return buf;
}

void save_changes (void)
{
   FILE *fp;
   int i;

   if (!(fp = fopen (CHANGES_FILE, "w")))
   {
      perror (CHANGES_FILE);
      return;
   }

   fprintf (fp, "%d\n", maxChanges);
   for (i = 0; i < maxChanges; i++)
   {
      fprintf (fp, "%s~\n", changes_table[i].change);
      fprintf (fp, "%s~\n", changes_table[i].coder);
      fprintf (fp, "%s~\n", changes_table[i].date);
      fprintf (fp, "%ld\n", changes_table[i].mudtime);
	  fprintf (fp, "%d\n", changes_table[i].imm);
	  fprintf (fp, "%s~\n", changes_table[i].type);
      fprintf (fp, "\n");
   }
   fclose (fp);

   return;
}

 void delete_change (int iChange)
{
   int i, j;
   NEWCHANGE_DATA *new_table;

   new_table = malloc (sizeof (NEWCHANGE_DATA) * maxChanges);

   if (!new_table)
      return;

   iChange--;
   if( iChange < 0 || iChange > maxChanges )
      return;

   for (i = 0, j = 0; i < maxChanges + 1; i++)
   {
      if (i != iChange)
      {
new_table[j] = changes_table[i];
j++;
      }
   }

   free (changes_table);
   changes_table = new_table;
   maxChanges--;
   save_changes ();
   return;
}
// Count how many changes in the last 2 days - Luther
int recent_changes( void )
{
   int i, count;

   count = 0;
   for( i = 0; i <= maxChanges; i++ )
   {
      /* 172800 is equal to 2 days in seconds */
      if( ( current_time - changes_table[i].mudtime ) > 172800 )
         continue;
      count++;
   }

   return count;   
}

// Take a changes date string and strip out the month value - Luther
int strip_change_month( char *date )
{
    int a = 0;
    int num = 0;
    int value = -1;
    int count = 1;
    int total = 1;
    if( date[0] == '\0' )
        return -1;

    char dbuf[MAX_INPUT_LENGTH];

    for( a = 0; a < strlen(date); a++ )
    {
	sprintf(dbuf,"%c",date[a]);
        num = 0;
        if( is_number(dbuf) )
        {
          num = atoi(dbuf);
          if( count == 1 )
            value = 10 * num;
          else if( count == 2 )
          {
            value += num;
            count = 1;
            continue;
          }
          count++;
          continue;
        }
        else if( date[a] == '/' )
        {
          if( total == 1 )
            return value;
          total++;
          value = 0;
          continue;
        }
    }
    return value;
}

// Take a changes date string and strip out the day value - Luther
int strip_change_day( char *date )
{
    int a = 0;
    int num = 0;
    int value = -1;
    int count = 1;
    int total = 1;
    if( date[0] == '\0' )
	return -1;

    char dbuf[MAX_INPUT_LENGTH];

    for( a = 0; a < strlen(date); a++ )
    {
        sprintf(dbuf,"%c",date[a]);
	num = 0;
	if( is_number(dbuf) )
	{
	  num = atoi(dbuf);
	  if( count == 1 )
	    value = 10 * num;
	  else if( count == 2 )
	  {
	    value += num;
	    count = 1;
	    continue;
	  }
	  count++;
	  continue;
	}
	else if( date[a] == '/' )
	{
	  if( total == 2 )
	    return value;
	  total++;
	  value = 0;
	  continue;
	}
    }
    return value;
}

// Take a changes date string and strip out the year value - Luther
int strip_change_year( char *date )
{
    int a = 0;
    int num = 0;
    int value = -1;
    int count = 1;
    int total = 1;
    if( date[0] == '\0' )
        return -1;

    char dbuf[MAX_INPUT_LENGTH];

    for( a = 0; a < strlen(date); a++ )
    {
        sprintf(dbuf,"%c",date[a]);
        num = 0;
        if( is_number(dbuf) )
        {
          num = atoi(dbuf);
          if( count == 1 )
            value = 10 * num;
          else if( count == 2 )
          {
            value += num;
            count = 1;
            continue;
          }
          count++;
          continue;
        }
        else if( date[a] == '/' )
        {
          if( total == 3 )
            return value;
          total++;
          value = 0;
          continue;
        }
    }
    return value;
}
void do_addimmchange (CHAR_DATA * ch, char *argument)
{
   CHAR_DATA *vch;
   NEWCHANGE_DATA *new_table;
   char buf[MAX_STRING_LENGTH];
   char arg[MAX_STRING_LENGTH];
   if (IS_NPC (ch))
      return;
   argument = one_argument(argument,arg);

   if ( !arg[0] )
   {
	   send_to_char( "No type specified\n\r",ch );
	   return;
   }

   if (str_cmp(arg, "code") && str_cmp(arg, "area") && str_cmp(arg, "misc") && str_cmp(arg,"clan") && str_cmp(arg,"help"))
   {
	   send_to_char( "Code/Area/Misc/Clan/Help please.\n\r", ch );
	   return;
   }

   if (argument[0] == '\0')
   {
      send_to_char ("Syntax: Addimmchange $ChangeString\n\r", ch);
      send_to_char ("Type 'changes imm' to view the list.&D\n\r", ch);
      return;
   }

   maxChanges++;
   new_table =
      realloc (changes_table, sizeof (NEWCHANGE_DATA) * (maxChanges + 1));

   if (!new_table)
   {				/* realloc failed */
      send_to_char ("Memory allocation failed. Brace for impact.\n\r", ch);
      return;
   }
   changes_table = new_table;
   changes_table[maxChanges - 1].change = str_dup (argument);
   changes_table[maxChanges - 1].coder = str_dup (ch->name);
   changes_table[maxChanges - 1].date = str_dup (current_date ());
   changes_table[maxChanges - 1].mudtime = current_time;
   changes_table[maxChanges - 1].imm = 1;
   changes_table[maxChanges - 1].type = str_dup (arg);

   for ( vch = first_char; vch != NULL; vch = vch->next )
   {
	if ( IS_NPC(vch) ) continue;
	if ( !IS_IMMORTAL(vch) )continue;
   sprintf (buf,
	    "&c%s &whas added a new Immortal-only change, type '&Ychanges imm&w' to see what it was",
	    ch->name);
   cent_to_char( buf,vch );
   }
   save_changes ();
   return;
}

void do_addchange (CHAR_DATA * ch, char *argument)
{
   NEWCHANGE_DATA *new_table;
   char buf[MAX_STRING_LENGTH];
   char arg[MAX_STRING_LENGTH];
   if (IS_NPC (ch))
      return;

   argument = one_argument(argument,arg);


   if (str_cmp(arg, "code") && str_cmp(arg, "area") && str_cmp(arg, "misc") && str_cmp(arg,"clan") && str_cmp(arg,"help"))
   {
	   send_to_char( "Code/Area/Misc/Clan/Help please.\n\r", ch );
	   return;
   }

   if (argument[0] == '\0')
   {
      send_to_char ("Syntax: Addchange <change desc>\n\r", ch);
      send_to_char ("Type 'changes' to view the list.&D\n\r", ch);
      return;
   }

   maxChanges++;
   new_table =
      realloc (changes_table, sizeof (NEWCHANGE_DATA) * (maxChanges + 1));

   if (!new_table)
   {				/* realloc failed */
      send_to_char ("Memory allocation failed. Brace for impact.\n\r", ch);
      return;
   }
   changes_table = new_table;
   changes_table[maxChanges - 1].change = str_dup (argument);
   changes_table[maxChanges - 1].coder = str_dup (ch->name);
   changes_table[maxChanges - 1].date = str_dup (current_date ());
   changes_table[maxChanges - 1].mudtime = current_time;
   changes_table[maxChanges - 1].imm = 0;
   changes_table[maxChanges - 1].type = str_dup (arg);
  
sprintf (buf,
	    "&c%s &Yhas added a new &c%s &Ychange, type &r'&zchanges&r'&W to see what it was",
	    ch->name, strupper(arg));
   do_info(ch ,buf);
   save_changes ();
   return;
}

void do_chedit (CHAR_DATA * ch, char *argument)
{
   char arg1[MAX_INPUT_LENGTH];
   char arg2[MAX_INPUT_LENGTH];
   char buf[MAX_STRING_LENGTH];

   argument = one_argument (argument, arg1);
   argument = one_argument (argument, arg2);

   if (IS_NPC (ch))
      return;

   if (!ch->desc || NULLSTR (arg1))
   {
      send_to_char ("Syntax: chedit load/save\n\r", ch);
      send_to_char ("Syntax: chedit delete (change number)\n\r", ch);
      send_to_char ("Syntax: chedit rename (change number)\n\r", ch);
      return;
   }
   else if (!str_cmp (arg1, "load"))
   {
      load_changes ();
      send_to_char ("Changes Loaded.\n\r", ch);
      return;
   }
   else if (!str_cmp (arg1, "save"))
   {
      save_changes ();
      send_to_char ("Changes Saved.\n\r", ch);
      return;
   }
   else if (!str_cmp (arg1, "delete"))
   {
      int num;

      if (NULLSTR (arg2) || !is_number (arg2))
      {
	 send_to_char
	    ("&wFor editchange delete, you must provide a change number.&D\n\r",
	     ch);
	 send_to_char ("Syntax: chsave delete (change number)\n\r", ch);
	 return;
      }

      num = atoi (arg2);

      if (num < 0 || num > maxChanges)
      {
	 sprintf (buf, "Valid changes are from 0 to %d.\n\r", maxChanges);
	 send_to_char (buf, ch);
	 return;
      }

      delete_change (num);
      send_to_char ("Change deleted.\n\r", ch);

      return;
   }
   else if (strcmp (arg1, "rename") == 0)
   {
      int num;
      if (NULLSTR (arg2) || !is_number (arg2) || NULLSTR (argument))
      {
	 send_to_char
	    ("&YFor chsave rename, you must choose a change number.&D\n\r",
	     ch);
	 send_to_char
	    ("Syntax: chsave rename (change number) 'New Change'&D\n\r",
	     ch);
	 return;
      }
      num = atoi (arg2);
      if (num < 0 || num > maxChanges)
      {
	 char buf[MAX_STRING_LENGTH];
	 sprintf (buf, "Valid changes are from 0 to %d.\n\r", maxChanges);
	 send_to_char (buf, ch);
	 return;
      }
      changes_table[num - 1].change = str_dup (argument);
      save_changes ();
      send_to_char ("Change renamed.\n\r", ch);
      return;
   }

}


char *line_indent (char *text, int wBegin, int wMax)
{
   static char buf[MAX_STRING_LENGTH];
   char *ptr;
   char *ptr2;
   int count = 0;
   bool stop = FALSE;
   int wEnd = 0;
   buf[0] = '\0';
   ptr = text;
   ptr2 = buf;

   while (!stop)
   {
      if (count == 0)
      {
	 if (*ptr == '\0')
	    wEnd = wMax - wBegin;
	 else if (strlen (ptr) < (wMax - wBegin))
	    wEnd = wMax - wBegin;
	 else
	 {
	    int x = 0;

	    while (*(ptr + (wMax - wBegin - x)) != ' ')
	       x++;
	    wEnd = wMax - wBegin - (x - 1);
	    if (wEnd < 1)
	       wEnd = wMax - wBegin;
	 }
      }
      if (count == 0 && *ptr == ' ')
	 ptr++;
      else if (++count != wEnd)
      {
	 if ((*ptr2++ = *ptr++) == '\0')
	    stop = TRUE;
      }
      else if (*ptr == '\0')
      {
	 stop = TRUE;
	 *ptr2 = '\0';
      }
      else
      {
	 int k;

	 count = 0;
	 *ptr2++ = '\n';
	 *ptr2++ = '\r';
	 for (k = 0; k < wBegin; k++)
	    *ptr2++ = ' ';
      }
   }
   return buf;
}
void nnum_changes (CHAR_DATA *ch)
{
	char buf[MAX_INPUT_LENGTH];
	char *test;
	int today;
	int i;

	i = 0;
	test = current_date ();
	today = 0;

	for (i = 0; i < maxChanges; i++)
	   if ( changes_table[i].imm > 0 && !IS_IMMORTAL(ch)) maxChanges--;
   
   i = 0;
   for (i = 0; i < maxChanges; i++)
   {
      if (!str_cmp (test, changes_table[i].date))
	  {
		today++;
	  }
   }

   if ( today > 0 )
   {
	   sprintf( buf, " &zThere have been &W%d&z changes added to the MUD today &z-&W%d&z- total&D\n\r&z Type &W'&Wchanges&W'&zto view them&D\n\r", today, maxChanges );
	   send_to_char( buf, ch );
	   //return;
   }
   load_changes();
   return;
}
int num_changes( void ) 
{
  char *test;
  int today;
  int i;

  i = 0;
  test = current_date(  );
  today = 0;

  for( i = 0; i < maxChanges; i++ )
    if( !str_cmp( test, changes_table[i].date ) )
      today++;
  return today;
}
void do_changes (CHAR_DATA * ch, char *argument)
{
   char arg[MAX_INPUT_LENGTH], arg2[MAX_INPUT_LENGTH], buf[MAX_STRING_LENGTH], *test;
   int i, page = 0, maxpage = 0, today;
   int display = 0;
   argument = one_argument (argument, arg);
   argument = one_argument (argument, arg2);

   if (IS_NPC (ch))
      return;
   if (maxChanges < 1)
      return;
 if( !str_cmp(arg,"recent") )
  {  
   send_to_char 
("&z\n\r&c+&W==============================================================================&c+\n\r", ch);
    sprintf( buf, "&WA total of &r[ &z%2d &r] &Wnew changes have been added today.&d\n\r", num_changes() );
    send_to_char( buf, ch );
    sprintf( buf, "&WA total of &r[ &z%2d &r] &Wchanges added in the last two days.&d\n\r", recent_changes());
    send_to_char( buf, ch );
    ch_printf(ch,"&WType &r'&zchanges&r' &Wto see a list of changes.\n\r");
    send_to_char ("&c+&W==============================================================================&c+\n\r", ch);
    return;
  }
   int fag = maxChanges;
   i = 0;
   test = current_date ();
   today = 0;

   for (i = 0; i < fag; i++)
	   if ( changes_table[i].imm > 0 && !IS_IMMORTAL(ch)) fag--;
   
   i = 0;

   for (i = 0; i < maxChanges; i++)
   {
      if (!str_cmp (test, changes_table[i].date))
	  {
		today++;
	  }
   }

   if (is_number (arg))
      page = atoi (arg);
   //maxpage = (maxChanges/10)+1;
   maxpage = (maxChanges + 9) / 10;
   if ( maxChanges == 0 )
   {
   stc("\n\r&RCurrently No changes have been added to the changes list", ch );
   return;
   }
   if (page > 0)
   {
      if (page > maxpage)
      {
	 sprintf (buf, "Show which page 1-%d\n\r", maxpage);
	 send_to_char (buf, ch);
	 return;
      }
      page *= 10;
   }
   send_to_char ("\n\r&YNUMBER &WIMMORTAL      &zTYPE    &RDATE        &cCHANGE                                        &D\n\r", ch);

   send_to_char ("&c+&W==============================================================================&c+\n\r", ch);

   if (!str_cmp (arg, "code") || !str_cmp (arg, "misc") || !str_cmp (arg, "area") || !str_cmp (arg, "help")|| !str_cmp (arg, "clan"))
   {
	   for( i = 0; i < maxChanges; i++ )
	   {
		   if( str_cmp(changes_table[i].type, arg) ) 
		   { 
			  continue; 
		   }

		   sprintf (buf, "&r[&Y%4d&r] &W%-13s &z%-4s &R%11s &r- &c%-55s\n\r",
               (i + 1),
               changes_table[i].coder,
			   strupper(changes_table[i].type),
               changes_table[i].date,
               line_indent (changes_table[i].change, 40, 79));
            send_to_char (buf, ch);
        }
   send_to_char ("&c+&W==============================================================================&c+\n\r", ch);
		if (today > 0)
			  sprintf (buf,
        	       "&WThere is a total of &r[&z%d&r]&W changes of which &r[&z%d&r] &W%s been added today.&D",
	               maxChanges, today, today > 1 ? "have" : "has");
        else
   sprintf (buf, "&WThere is a total of &r[&z%d&r]&W changes.&D", fag);
   cent_to_char (buf, ch);
   if ( IS_IMMORTAL(ch) ) cent_to_char ("&WBe sure to check &z<&Ychanges imm&r>&W!&d", ch );
   send_to_char ("&c+&W==============================================================================&c+\n\r", ch);
   sprintf (buf, "&WTo see pages of changes use: &r'&zchanges &z<&Y1&w-&O%d&w&z>&r'&D",
            (maxChanges + 9) / 10);
   cent_to_char (buf, ch);
   cent_to_char
      ("&WTo search all changes for a change use: &r'&Ychanges search &z<&rword&z>&r'&D",
       ch);
   send_to_char ("&c+&W==============================================================================&c+\n\r", ch);
   load_changes();
   return;
  }
   if (!str_cmp (arg, "imm"))
   {
	   if ( !IS_IMMORTAL(ch) )
	   {
			sprintf (buf, "&r[&zNone&r] &YNone &zNone &RNone &r- &cNone&d\n\r");
			 send_to_char (buf, ch);
			 return;
	   }
	   for ( i = 0; i < maxChanges; i++ )
	   {
		   if ( changes_table[i].imm != 1 ) continue;

		   if (page == 0
	  && changes_table[i].mudtime + (2 * 24L * 3600L) < current_time)
	 continue;
      display++;

      if (page > 0 && (page > 0 && (i < (page - 10) || i >= page)))
	 continue;

      sprintf (buf, "&r[&Y%4d&r] &W%-13s &z%-4s &Y%11s &r- &c%-55s &r[&YIMMCHANGE&r]&D\n\r",
	       (i + 1),
	       changes_table[i].coder,
		   strupper(changes_table[i].type),
	       changes_table[i].date,
	       line_indent (changes_table[i].change, 40, 79));
      send_to_char (buf, ch);
   }
   send_to_char ("&c+&W==============================================================================&c+\n\r", ch);
   if (today > 0)
      sprintf (buf,
	       "&WThere is a total of &r[&z%d&r] &Wchanges of which&r [&Y%d&r] &W%s been added today.&D",
	       maxChanges, today, today > 1 ? "have" : "has");
   else
      sprintf (buf, "&WThere is a total of &r[&z%d&r]&W changes.&D", maxChanges);
   cent_to_char (buf, ch);
   send_to_char ("&c+&W==============================================================================&c+\n\r", ch);
   sprintf (buf, "&WTo see pages of changes use: &r'&Ychanges &z<&r1&w-&Y%d&z>&r'",(maxChanges + 9) / 10);
   cent_to_char (buf, ch);
   cent_to_char ("&WTo search all changes for a change use: &r'&Ychanges search&w &z<&rword&z>&r'&D", ch);
   send_to_char ("&c+&W==============================================================================&c+\n\r", ch);
   return;
   }


   if (!str_cmp (arg, "search"))
   {
      int dsp = 0;

      if (arg2[0] == '\0')
      {
	 send_to_char ("What do you want to search for??\n\r", ch);
	 return;
      }

      for (i = 0; i < maxChanges; i++)
      {
	 if (!str_infix (arg2, changes_table[i].change)
	     || !str_infix (arg2, changes_table[i].coder))

	 {
	    sprintf (buf, "&r[&Y%4d&r] &W%-13s &z%-4s &R%11s &r- &W%-52s\n\r",
		     (++dsp),
		     changes_table[i].coder,
			 strupper(changes_table[i].type),
		     changes_table[i].date, 
		     line_indent (changes_table[i].change, 40, 79));
	    send_to_char (buf, ch);
	 }
      }
      if (dsp == 0)
	 cent_to_char ("There is NO match with what you have entered!.", ch);
   send_to_char ("&c+&W==============================================================================&c+\n\r", ch);
      return;
   }

   if ( !str_cmp(arg, "all") )
   {
	for (i = 0; i < maxChanges; i++)
	{
	    sprintf (buf, "&r[&Y%4d&r] &W%-13s &z%-4s &R%11s &r- &c%-55s\n\r",
               (i + 1),
               changes_table[i].coder,
			   strupper(changes_table[i].type),
               changes_table[i].date,
               line_indent (changes_table[i].change, 40, 79));
            send_to_char (buf, ch);
        }
   send_to_char ("&c+&W==============================================================================&c+\n\r", ch);
	if (today > 0)
	     sprintf( buf,  "&WThere is a total of &r[&z%d&r] &Wchanges of which&r [&Y%d&r] &W%s&W been added today.&D",
	       maxChanges, today, today > 1 ? "have" : "has");
        else
   sprintf (buf, "&WThere is a total of &r[&Y%d&r]&W changes.&D", fag);
   cent_to_char (buf, ch);
   if ( IS_IMMORTAL(ch) ) cent_to_char ("&WBe sure to check &'Rchanges imm'&w!", ch );
   send_to_char ("&c+&W==============================================================================&c+\n\r", ch);
   sprintf (buf, "&WTo see pages of changes use: &r'&Ychanges &z<&R1&w-&Y%d&z>&r'",
            (maxChanges + 9) / 10);
   cent_to_char (buf, ch);
   cent_to_char
      ("&WTo search all changes for a change use: '&Ychanges search &r<&zword&r>&W'&D",
       ch);
   send_to_char ("&c+&W==============================================================================&c+\n\r", ch);
   load_changes();
   return;
  }


   for (i = 0; i < maxChanges; i++)
   {
      if (page == 0
	  && changes_table[i].mudtime + (2 * 24L * 3600L) < current_time)
	 continue;
	  if (changes_table[i].imm != 0 ) continue;
      display++;

      if (page > 0 && (page > 0 && (i < (page - 10) || i >= page)))
	 continue;

      sprintf (buf, "&r[&Y%4d&r] &W%-13s &z%-4s &R%11s &r- &c%-55s\n\r",
	       (i + 1),
	       changes_table[i].coder,
		   strupper(changes_table[i].type),
	       changes_table[i].date,
	       line_indent (changes_table[i].change, 40, 79));
      send_to_char (buf, ch);
   }
   send_to_char ("&c+&W==============================================================================&c+\n\r", ch);
   if (today > 0)
      sprintf (buf,
	       "&WThere is a total of &r[&z%d&r] &Wchanges of which&r [&Y%d&r] &W%s&W been added today.&D",
	       maxChanges, today, today > 1 ? "have" : "has");
   else
      sprintf (buf, "&WThere is a total of &r[&Y%d&r]&W changes.&D", fag);
   cent_to_char (buf, ch);
   if ( IS_IMMORTAL(ch) ) cent_to_char ("&WBe sure to check '&Rchanges imm'&w!", ch );
   send_to_char ("&c+&W==============================================================================&c+\n\r", ch);
   sprintf (buf, "&WTo see pages of changes use: &r'&Ychanges &z<&R1&w-&Y%d&z>&r'",
            (maxChanges + 9) / 10);
   cent_to_char (buf, ch);
   cent_to_char
      ("&WTo search all changes for a change use: '&Ychanges search &r<&zword&r>&W'&D",
       ch);
   send_to_char ("&c+&W==============================================================================&c+\n\r", ch);
   load_changes();

}

