#include <sys/types.h>
#include <ctype.h>
#include <math.h>
#include <stdio.h>
#include <string.h>
#include <time.h>
#include "mud.h"
//Pl mod by Hokai - fixed by kalisto.

float global_plgain;

void adjust_global_gains( CHAR_DATA *ch, bool msg )
{
    CHAR_DATA *vch;
    float mod = 0;
    char buf[MAX_STRING_LENGTH];

    global_plgain = 2.0;

    for( vch = first_char; vch; vch = vch->next )
    {
	if( IS_NPC(vch) )
	  continue;
	if( !vch->desc )
	  continue;
	if( IS_IMMORTAL(vch) )
	  continue;

	if( ( vch->desc->connected == CON_PLAYING
          || vch->desc->connected == CON_EDITING )
          || ( vch->desc->connected >= CON_NOTE_TO 
	       && vch->desc->connected <= CON_NOTE_FINISH ) )
	{
	  if( global_plgain > 0 )
	      mod = 2;
	    else if( global_plgain >= 3 )
	      mod = 2.25;
	    else if( global_plgain >= 5 )
	      mod = 2.50;
            else if( global_plgain >= 7 )
              mod = 2.75;
            else if( global_plgain >= 9 )
              mod = 3.25;
            else if( global_plgain >= 12 )
              mod = 3.75;
            else if( global_plgain >= 15 )
              mod = 4.50;

	    else
	      mod = 1;
	    
	    global_plgain += mod;
            if (global_plgain > 100)
                global_plgain = 100;
	    continue;
	}
    }

    if( msg )
    {
      sprintf(buf,"&BGlobal gains are now affected by &R%.2f%%&B.",
                global_plgain );
      do_info(supermob,buf);
    }

    return;
} 

void do_plmod( CHAR_DATA * ch, char *argument )
{  
char buf[MAX_STRING_LENGTH];
adjust_global_gains(NULL,FALSE);
update_handler(  );
sprintf(buf,"&BGlobal gains are now affected by &R%.2f%%&B.", global_plgain );
do_info( ch, buf );
  return;
}


