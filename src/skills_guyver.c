/*******************************************/
/*  Guyver Transformations v1.0            */
/*             By Zynon                    */
/*                                         */
/*                                         */
/*                                         */
/*******************************************/

#include <sys/types.h>
#include <ctype.h>
#include <math.h>
#include <stdio.h>
#include <string.h>
#include <time.h>
#include "mud.h"

extern void transStatApply args( ( CHAR_DATA * ch, int strMod, int spdMod, int intMod, int conMod ) );
extern void transStatRemove args( ( CHAR_DATA * ch ) );

void do_guyver( CHAR_DATA * ch, char *argument )
{

  if( IS_NPC( ch ) && is_split( ch ) )
  {
    if( !ch->master )
      return;
    if( !can_use_skill( ch->master, number_percent(  ), gsn_guyver ) )
      return;
  }

  if( xIS_SET( ( ch )->affected_by, AFF_GUYVER ) )
  {
   send_to_char( "You power down and return to normal.\n\r", ch );
    xREMOVE_BIT( ( ch ) ->affected_by, AFF_GUYVER );
    ch->pl = ch->exp;
    transStatRemove( ch );
 if( !IS_NPC( ch ) )
    {
      ch->pcdata->haircolor = ch->pcdata->orignalhaircolor;
      ch->pcdata->eyes = ch->pcdata->orignaleyes;
      
    }

    return;
}
  if( ch->mana < skill_table[gsn_guyver]->min_mana )
  {
    send_to_char( "You don't have enough energy.\n\r", ch );
    return;
  }

  WAIT_STATE( ch, skill_table[gsn_guyver]->beats );
  if( can_use_skill( ch, number_percent(  ), gsn_guyver ) )
  {
if(!can_give_light_effect(ch))
      ch->in_room->light++;

    act( AT_GREY, 
	"Your neck starts to pulse rapidly along with your heart beat when suddenly black tentacles come out of your neck. Wrapping around your body, the tentacles form a thick layer of armor overhead, as it slams firmly against your body. Your helmet forms over your head as compressed air shoots out from your mask as the ball on your forehead pulses with power.", ch, NULL, NULL,TO_CHAR );
    act( AT_YELLOW, "$n's neck starts to pulse rapidly along with $m heart beat when suddenly black tentacles come out of $s neck. Wrapping around your body, the tentacles form a thick layer of armor overhead, as it slams firmly against $n's body. $n's helmet forms over $s head as compressed air shoots out from $s mask as the ball on $s forehead pulses with power.", ch, NULL, NULL,         TO_NOTVICT );
    xSET_BIT( ( ch )->affected_by, AFF_GUYVER );
    ch->pl = ch->exp * 30;
    transStatApply( ch, 4300, 4700, 3800, 3700 );
    learn_from_success( ch, gsn_guyver );
 if( !IS_NPC( ch ) )
    {
      ch->pcdata->eyes = 0;
      ch->pcdata->haircolor = 3;
     
    }

}

 else
 
    switch ( number_range( 1, 3 ) )
    {
  default:
        act( AT_BLUE, "You scream out 'GGGGGUUUUYYYYVVVVEEERRRR', but nothing happens.", ch, NULL, NULL, TO_CHAR );
        act( AT_BLUE, "$n's screams out but nothing happens.", ch, NULL, NULL, TO_NOTVICT );
        break;
      case 1:
        act( AT_BLUE, "You scream out 'BIIIIOOOOO BOOOOOOOST!!', but nothing happens.", ch, NULL, NULL, TO_CHAR );
        act( AT_BLUE, "$n's screams out but nothing happens.", ch, NULL, NULL, TO_NOTVICT );
        break;
      case 2:
        act( AT_BLUE, "You scream out GGGGGUUUUYYYYVVVVEEERRRR, but nothing happens.", ch, NULL, NULL, TO_CHAR );
        act( AT_BLUE, "$n's screams out but nothing happens.", ch, NULL, NULL, TO_NOTVICT );
        break;
      case 3:
        act( AT_BLUE, "You scream out 'BIIIIOOOOO BOOOOOOOST!!', but nothing happens.", ch, NULL, NULL, TO_CHAR );
        act( AT_BLUE, "$n's screams out but nothing happens", ch, NULL, NULL, TO_NOTVICT );
        break;

    }

    ch->mana -= skill_table[gsn_guyver]->min_mana;
    return;
  }


/*******************************************/
/*  Guyver     Skills      Version 1.0     */ 
/*             By Zynon                    */
/*                                         */
/*                                         */
/* 							      */
/*******************************************/



