/*******************************************/
/*  Shinigami Transformations Version 1.0  */
/*             By Kalisto                  */
/*                                         */
/* [\] Shikai          [0] Vizard          */
/* [0] Bankai          [0] True Hollow     */
/*******************************************/

#include <sys/types.h>
#include <ctype.h>
#include <math.h>
#include <stdio.h>
#include <string.h>
#include <time.h>
#include "mud.h"

extern void transStatApply args( ( CHAR_DATA * ch, int strMod, int spdMod, int intMod, int conMod ) );
extern void transStatRemove args( ( CHAR_DATA * ch ) );

void do_shikai( CHAR_DATA * ch, char *argument )
{

  if( IS_NPC( ch ) && is_split( ch ) )
  {
    if( !ch->master )
      return;
    if( !can_use_skill( ch->master, number_percent(  ), gsn_shikai ) )
      return;
  }

  if( wearing_chip( ch ) )
  {
    ch_printf( ch, "You can't while you have a chip installed.\n\r" );
    return;
  }

  if( wearing_sentient_chip( ch ) )
  {
    ch_printf( ch, "You can't use this." );
    return;
  }

  double pl_mult = 0;
  int arg;

  arg = atoi( argument );
if( xIS_SET( ( ch )->affected_by, AFF_SHIKAI ) && arg == 0 )
  {
    send_to_char( "Your Zanpakuto returns to its original form as you put it back into its sheath.\n\r", ch );
    xREMOVE_BIT( ( ch )->affected_by, AFF_SHIKAI );
    ch->in_room->light--;
    transStatRemove( ch );
    ch->pl = ch->exp;
    if( !IS_NPC( ch ) )
    {
      ch->pcdata->haircolor = ch->pcdata->orignalhaircolor;
      ch->pcdata->eyes = ch->pcdata->orignaleyes;
    }
    return;
  }
  else if( !xIS_SET( ( ch )->affected_by, AFF_SHIKAI ) && arg == 0 )
  {
    ch_printf( ch, "Syntax: 'Shikai' [2-15]\n\r" );
    return;
  }

  pl_mult = 15;

  if( arg > pl_mult )
    pl_mult = ( int )pl_mult;
  else if( arg < 1 )
    pl_mult = 1;
  else
    pl_mult = arg;

  if( ch->mana < skill_table[gsn_shikai]->min_mana * pl_mult )
  {
    send_to_char( "&RYou still can not hear your zanpakutos name to call it out.&D\n\r", ch );
    return;
  }
  /*
   * Added this to curve confusion about the Mystic PL_MULT -Karn 01.29.05
   */
  if( arg < 2 || arg > 15 )
  {
    send_to_char( "Syntax: 'shikai' [2-15]\n\r", ch );
    return;
  }
  WAIT_STATE( ch, skill_table[gsn_shikai]->beats );
  if( can_use_skill( ch, number_percent(  ), gsn_shikai ) )
  {
    act( AT_CYAN,
         "&CYou pull your zanpakuto out and you hollar its name, when you do reiatsu forms around you like an aura of mystical divination, as your zanpakuto completes its shikai release.",
         ch, NULL, NULL, TO_CHAR );

    act( AT_CYAN,
         "&C$n Screams out the name of his zanpakuto as he draws it from his sheath it undergoes a beatiful transformation and the reiatsu builds around $n like a aura of mystical divination.",
         ch, NULL, NULL, TO_NOTVICT );

    xSET_BIT( ( ch )->affected_by, AFF_SHIKAI );
      xREMOVE_BIT((ch)->affected_by, AFF_BANKAI);
     xREMOVE_BIT((ch)->affected_by, AFF_VIZARD);
    ch->in_room->light++;

    ch->pl = ch->exp * pl_mult;

    transStatApply( ch, pl_mult * 11.5, pl_mult * 11.5, pl_mult, pl_mult * 11.5 );

    if( xIS_SET( ( ch )->affected_by, AFF_MAJIN ) )
      ch->pl = ch->pl * 1.1;
  ch->mana -= skill_table[gsn_shikai]->min_mana;
    learn_from_success( ch, gsn_shikai );
  }
  else
  {
    act( AT_RED,
         "Your zanpakuto refuses to go into its shikai release cause you mispronounced its name.",
         ch, NULL, NULL, TO_CHAR );
    act( AT_RED,
         "$n stands there looking stupid shaking his sword around like it was supposed to do something.",
         ch, NULL, NULL, TO_NOTVICT );
    learn_from_failure( ch, gsn_shikai );
  ch->mana -= skill_table[gsn_shikai]->min_mana;

  }

//  ch->mana -= skill_table[gsn_shikai]->min_mana * pl_mult;
  ch->mana -= skill_table[gsn_shikai]->min_mana;

  return;
}

void do_bankai( CHAR_DATA * ch, char *argument )
{

  if( IS_NPC( ch ) && is_split( ch ) )
  {
    if( !ch->master )
      return;
    if( !can_use_skill( ch->master, number_percent(  ), gsn_bankai ) )
      return;
  }

  if( wearing_chip( ch ) )
  {
    ch_printf( ch, "You can't while you have a chip installed.\n\r" );
    return;
  }

  if( wearing_sentient_chip( ch ) )
  {
    ch_printf( ch, "You can't use this." );
    return;
  }

  double pl_mult = 0;
  int arg;

  arg = atoi( argument );

  if( xIS_SET( ( ch )->affected_by, AFF_BANKAI ) && arg == 0 )
  {
    send_to_char( "You release your bankai.\n\r", ch );
    ch->in_room->light--;
    xREMOVE_BIT( ( ch )->affected_by, AFF_BANKAI );
    transStatRemove( ch );
    ch->pl = ch->exp;
    if( !IS_NPC( ch ) )
    {
      ch->pcdata->haircolor = ch->pcdata->orignalhaircolor;
      ch->pcdata->eyes = ch->pcdata->orignaleyes;
    }
    return;
  }
  else if( !xIS_SET( ( ch )->affected_by, AFF_BANKAI ) && arg == 0 )
  {
    ch_printf( ch, "Syntax: 'bankai' [16-28]\n\r" );
    return;
  }

  pl_mult = 28;

  if( arg > pl_mult )
    pl_mult = ( int )pl_mult;
  else if( arg < 1 )
    pl_mult = 1;
  else
    pl_mult = arg;

  if( ch->mana < skill_table[gsn_bankai]->min_mana * pl_mult )
  {
    send_to_char( "Not enough Reiatsu.&D\n\r", ch );
    return;
  }
  if( arg < 16 || arg > 28 )
  {
    send_to_char( "Syntax: 'bankai' [16-28]\n\r", ch );
    return;
  }
  WAIT_STATE( ch, skill_table[gsn_power_form]->beats );
  if( can_use_skill( ch, number_percent(  ), gsn_bankai ) )
  {
    act( AT_GREEN,
         "You grip the handle of your zanpakuto tightly, raising it at the ready. Then you utter the phrase, Ban-kai! Suddenly your reiatsu sky rockets as your zanpakuto's spirit manifests itself, releasing its true power.  ",
	ch, NULL, NULL, TO_CHAR );

    act( AT_GREEN,
         "$n grips the handle of his zanpakuto tightly, raising it at the ready. $n utters the phrase, Bankai! Suddenly $n's reiatsu sky rockets as his zanpakuto's spirit manifests itself, releasing its true power.",
	ch, NULL, NULL, TO_NOTVICT );

    xSET_BIT( ( ch )->affected_by, AFF_BANKAI );
      xREMOVE_BIT((ch)->affected_by, AFF_SHIKAI);
      xREMOVE_BIT((ch)->affected_by, AFF_VIZARD);
    ch->in_room->light++;

    ch->pl = ch->exp * pl_mult;

    transStatApply( ch, pl_mult * 12.5, pl_mult * 12.5, pl_mult * 12.5, pl_mult );

    if( xIS_SET( ( ch )->affected_by, AFF_MAJIN ) )
      ch->pl = ch->pl * 1.1;

    learn_from_success( ch, gsn_bankai );
  }
  else
  {
    act( AT_GREEN,
         "You have a hard time unleashing your Zanpakuto's bankai.",
         ch, NULL, NULL, TO_CHAR );
    act( AT_GREEN,
         "$n appears to be having a hard time unleashing his Zanpakuto's bankai.",
         ch, NULL, NULL, TO_NOTVICT );
    learn_from_failure( ch, gsn_active_form );
  }

  ch->mana -= skill_table[gsn_bankai]->min_mana * pl_mult;
  return;
}


void do_vizard (CHAR_DATA * ch, char *argument )
{

  if( IS_NPC( ch ) && is_split( ch ) )
  {
    if( !ch->master )
      return;
    if( !can_use_skill( ch->master, number_percent(  ), gsn_vizard ) )
      return;
  }

  if( wearing_chip( ch ) )
  {
    ch_printf( ch, "You can't while you have a chip installed.\n\r" );
    return;
  }

  if( wearing_sentient_chip( ch ) )
  {
    ch_printf( ch, "You can't use this." );
    return;
  }

  double pl_mult = 0;
  int arg;

  arg = atoi( argument );

  if( xIS_SET( ( ch )->affected_by, AFF_VIZARD ) && arg == 0 )
  {
    send_to_char( "You have returned to normal\n\r", ch );
    ch->in_room->light--;
    xREMOVE_BIT( ( ch )->affected_by, AFF_VIZARD );
    transStatRemove( ch );
    ch->pl = ch->exp;
    if( !IS_NPC( ch ) )
    {
      ch->pcdata->haircolor = ch->pcdata->orignalhaircolor;
      ch->pcdata->eyes = ch->pcdata->orignaleyes;
    }
    return;
  }
  else if( !xIS_SET( ( ch )->affected_by, AFF_VIZARD ) && arg == 0 )
  {
    ch_printf( ch, "Syntax: 'vizard' [20-37]\n\r" );
    return;
  }

  pl_mult = 37;

  if( arg > pl_mult )
    pl_mult = ( int )pl_mult;
  else if( arg < 1 )
    pl_mult = 1;
  else
    pl_mult = arg;

  if( ch->mana < skill_table[gsn_vizard]->min_mana * pl_mult )
  {
    send_to_char( "Not enough Reiatsu.&D\n\r", ch );
    return;
  }
   if( arg < 20 || arg > 37 )
  {
    send_to_char( "Syntax: 'vizard' [20-37]\n\r", ch );
    return;
  }
  WAIT_STATE( ch, skill_table[gsn_vizard]->beats );
  if( can_use_skill( ch, number_percent(  ), gsn_vizard ) )
  {
    act( AT_CYAN,
         "A hollow mask begins to form on your face as you recede into yourself, letting rage and despair over come you.  Your reiatsu surges outwards as you harness those intentions and complete your hollowfication.",
         ch, NULL, NULL, TO_CHAR );

    act( AT_CYAN,
         "A hollow mask begins to form on $n's face as rage and despair overcomes him.  His reiatsu surges outwards as he harnesses those intentions and he completes his hollowfication.",
         ch, NULL, NULL, TO_NOTVICT );

    xSET_BIT( ( ch )->affected_by, AFF_VIZARD );
      xREMOVE_BIT((ch)->affected_by, AFF_BANKAI);
      xREMOVE_BIT((ch)->affected_by, AFF_SHIKAI);
    ch->in_room->light++;

    ch->pl = ch->exp * pl_mult;

    transStatApply( ch, pl_mult * 13.5, pl_mult * 13.5, pl_mult * 13.5, pl_mult * 13.5 );

    if( xIS_SET( ( ch )->affected_by, AFF_MAJIN ) )
      ch->pl = ch->pl * 1.1;

    learn_from_success( ch, gsn_proto_form );
  }
  else
  {
    act( AT_CYAN,
         "As hollow mask begins forming on your face but quickly deteriorates.  ",
         ch, NULL, NULL, TO_CHAR );
    act( AT_CYAN,
         "A hollow mask begins forming on $n's face but quickly deteriorates. ",
         ch, NULL, NULL, TO_NOTVICT );
    learn_from_failure( ch, gsn_vizard );
  }

  ch->mana -= skill_table[gsn_vizard]->min_mana * pl_mult;
  return;
}


/*
void do_true_hollow( CHAR_DATA * ch, char *argument )
{

  if( IS_NPC( ch ) && is_split( ch ) )
  {
    if( !ch->master )
      return;
    if( !can_use_skill( ch->master, number_percent(  ), gsn_true_hollow ) )
      return;
  }

  if( wearing_chip( ch ) )
  {
    ch_printf( ch, "You can't while you have a chip installed.\n\r" );
    return;
  }

  if( wearing_sentient_chip( ch ) )
  {
    ch_printf( ch, "You can't use this." );
    return;
  }

  double pl_mult = 0;
  int arg;

  arg = atoi( argument );


 if( xIS_SET( ( ch )->affected_by, AFF_TRUE_HOLLOW ) && arg == 0 )
  {
    send_to_char( "True Hollow transformation complete. (We need a transformation message send us one).\n\r", ch );
    ch->in_room->light--;
    xREMOVE_BIT( ( ch )->affected_by, AFF_TRUE_HOLLOW );
    transStatRemove( ch );
    ch->pl = ch->exp;
    if( !IS_NPC( ch ) )
    {
      ch->pcdata->haircolor = ch->pcdata->orignalhaircolor;
      ch->pcdata->eyes = ch->pcdata->orignaleyes;
    }
    return;
  }
  else if( !xIS_SET( ( ch )->affected_by, AFF_TRUE_HOLLOW ) && arg == 0 )
  {
    ch_printf( ch, "Syntax: 'true hollow' [2-45]\n\r" );
    return;
  }

  pl_mult = 45;

  if( arg > pl_mult )
    pl_mult = ( int )pl_mult;
  else if( arg < 1 )
    pl_mult = 1;
  else
    pl_mult = arg;

  if( ch->mana < skill_table[gsn_true_hollow]->min_mana * pl_mult )
  {
    send_to_char( "&RNot enough REIATSU.&D\n\r", ch );
    return;
  }

  if( arg < 2 || arg > 45 )
  {
    send_to_char( "Syntax: 'true hollow' [2-45]\n\r", ch );
    return;
  }
  WAIT_STATE( ch, skill_table[gsn_true_hollow]->beats );
  if( can_use_skill( ch, number_percent(  ), gsn_true_hollow ) )
  {

    {
      act( AT_DGREY,
           "True Hollow transformation complete. (We need a transformation message send us one).",
           ch, NULL, NULL, TO_CHAR );
      act( AT_DGREY,
           "True Hollow transformation complete. (We need a transformation message send us one).",
           ch, NULL, NULL, TO_NOTVICT );
    }

    xSET_BIT( ( ch )->affected_by, AFF_TRUE_HOLLOW );
      xREMOVE_BIT((ch)->affected_by, AFF_BANKAI);
      xREMOVE_BIT((ch)->affected_by, AFF_SHIKAI);
      xREMOVE_BIT((ch)->affected_by, AFF_VIZARD);
    ch->in_room->light++;

    ch->pl = ch->exp * pl_mult;

    transStatApply( ch, pl_mult * 15.5, pl_mult * 15.5, pl_mult * 15.5, pl_mult * 15.5 );

    if( xIS_SET( ( ch )->affected_by, AFF_MAJIN ) )
      ch->pl = ch->pl * 1.1;

    learn_from_success( ch, gsn_true_hollow );
  }
  else
  {
    act( AT_DGREY,
         "Your body fails to re-proportion your power right to unlock your Omega Form.",
         ch, NULL, NULL, TO_CHAR );
    act( AT_DGREY,
         "$n's armor phase-shifts slightly but returns to normal as $n fails to unlock $s Form.",
         ch, NULL, NULL, TO_NOTVICT );
    learn_from_failure( ch, gsn_true_hollow );
  }

  ch->mana -= skill_table[gsn_true_hollow]->min_mana * pl_mult;
  return;
}

*/
/*******************************************/
/*  Shinigami     Skills      Version 1.0  */
/*             By Kalisto                  */
/*                                         */
/* [ ] Shakkaho        [X] Raikoho         */
/* [X] Sokatsui        [> ] Rikujokoro     */
/*                                         */
/*******************************************/


void do_rikujokoro(CHAR_DATA * ch, char *argument)
{
  AFFECT_DATA af;
  CHAR_DATA *vch, *vch_next;

  if (IS_NPC(ch) && is_split(ch)) {
    return;
  }

  if (!IS_NPC(ch)
      && ch->exp < skill_table[gsn_rikujokoro]->skill_level[ch->class]) {
    send_to_char("You can't do that.\n\r", ch);
    return;
  }
  if (xIS_SET(ch->in_room->room_flags, ROOM_ARENA)
      || xIS_SET(ch->in_room->room_flags, ROOM_SAFE)) {
    send_to_char("There's no need to use that here.\n\r", ch);
    return;
  }
  if (who_fighting(ch) == NULL) {
    send_to_char("You aren't fighting anyone.\n\r", ch);
    return;
  }
  if (ch->mana < skill_table[gsn_rikujokoro]->min_mana) {
    send_to_char("You don't have enough energy.\n\r", ch);
    return;
  }
  if (ch->focus < skill_table[gsn_rikujokoro]->focus) {
    send_to_char("You need to focus more.\n\r", ch);
    return;
  } else
    ch->focus -= skill_table[gsn_rikujokoro]->focus;

  WAIT_STATE(ch, skill_table[gsn_rikujokoro]->beats);

  if (can_use_skill(ch, number_percent(), gsn_rikujokoro)) {
    act(AT_CYAN,
        "You Begin Reciting 'Carriage of thunder, bridge of a spinning wheel",
        ch, NULL, NULL, TO_CHAR);
    act(AT_CYAN,
        "with light, divide this into six. Bakudo Number 61",
        ch, NULL, NULL, TO_CHAR);
    act(AT_CYAN,
        "Rikujokoro!'",
        ch, NULL, NULL, TO_CHAR);
    act(AT_YELLOW,
        "You point your index finger at your enemie as a bright flash of energy",
        ch, NULL, NULL, TO_CHAR);
    act(AT_YELLOW,
        "Emminates from your fingertip and shoots towards them",
        ch, NULL, NULL, TO_CHAR);
    act(AT_YELLOW,
        "The energy then splits into 6 beams and slams into $t's torso",
        ch, NULL, NULL, TO_CHAR);
    act(AT_YELLOW, "holding them in place making them unable to move momentarily.", ch, NULL,
        NULL, TO_CHAR);

    act(AT_YELLOW,
        "$n Begins Reciting &C'Carriage of thunder, bridge of a spinning wheel",
        ch, NULL, NULL, TO_NOTVICT);
    act(AT_CYAN,
        "with light, divide this into six. Bakudo number 61",
        ch, NULL, NULL, TO_NOTVICT);
    act(AT_CYAN,
        "Rikujokoro!",
        ch, NULL, NULL, TO_NOTVICT);
    act(AT_YELLOW,
        "$n points $s index finger at their enemie as a bright flash of energy",
        ch, NULL, NULL, TO_NOTVICT);
    act(AT_YELLOW,
        "Emminates from $n's fingertip and shoots towards them.",
        ch, NULL, NULL, TO_NOTVICT);
    act(AT_YELLOW,
        "The energy then splits into 6 beams and slams into $s torso",
        ch, NULL, NULL, TO_NOTVICT);
    act(AT_YELLOW, "holding them in place making them unable to move momentarily.", ch, NULL,
        NULL, TO_NOTVICT);

        act(AT_CYAN,
        "$n Begins Reciting 'Carriage of thunder, bridge of a spinning wheel",
        ch, NULL, NULL, TO_VICT);
    act(AT_CYAN,
        "with light, divide this into six. Bakudo Number 61",
        ch, NULL, NULL, TO_VICT);
    act(AT_CYAN,
        "Rikujokoro!'",
        ch, NULL, NULL, TO_VICT);
    act(AT_YELLOW,
        "$n points $s index finger at you as a bright flash of energy",
        ch, NULL, NULL, TO_VICT);
    act(AT_YELLOW,
        "Emminates from $s fingertip and shoots towards you.",
        ch, NULL, NULL, TO_VICT);
    act(AT_YELLOW,
        "The energy then splits into 6 beams and slams into your torso",
        ch, NULL, NULL, TO_VICT);
    act(AT_YELLOW, "holding you in place making them unable to move momentarily.", ch, NULL,
        NULL, TO_VICT);

    learn_from_success(ch, gsn_rikujokoro);
    for (vch = ch->in_room->first_person; vch; vch = vch_next) {

      vch_next = vch->next_in_room;
      if (!IS_AWAKE(vch))
        continue;

      /*
       * if( number_range( 1, 100 ) < ( get_curr_int( vch ) / 10 ) )
       * {
       * send_to_char( "&zYou dodge all the energy rings tossed at you.\n\r", vch );
       * continue;
       * }
       */

      if (ch != vch && who_fighting(vch) != NULL) {
        af.type = gsn_rikujokoro;
        af.duration = 3;
        af.location = APPLY_HITROLL;
        af.modifier = -100;
        af.bitvector = meb(AFF_CHARM);
        affect_to_char(vch, &af);
      }

    }

  } else {
    act(AT_WHITE,
        "You begin to charge the energy but it fizzles out as you toss it.",
        ch, NULL, NULL, TO_CHAR);
    act(AT_WHITE, "$n misses with their energy rings.", ch, NULL, NULL,
        TO_NOTVICT);
    learn_from_failure(ch, gsn_rikujokoro);
  }
  ch->mana -= skill_table[gsn_rikujokoro]->min_mana;
  return;
}

void do_sokatsui(CHAR_DATA *ch, char *argument ) /* Sokatsui */
{
    CHAR_DATA *victim;
    int dam = 0;

    if( IS_NPC(ch) && is_split(ch) )
    {
        if( !ch->master )
          return;
        if( !can_use_skill( ch->master, number_percent(), gsn_sokatsui ))
          return;
    }

    if ( IS_NPC(ch) && IS_AFFECTED( ch, AFF_CHARM ) )
    {
	send_to_char( "You can't concentrate enough for that.\n\r", ch );
	return;
    }

    if ( !IS_NPC(ch)
    &&   ch->exp < skill_table[gsn_sokatsui]->skill_level[ch->class] )
    {
	send_to_char("You can't do that.\n\r", ch );
	return;
    }

    if ( ( victim = who_fighting( ch ) ) == NULL )
    {
	send_to_char( "You aren't fighting anyone.\n\r", ch );
	return;
    }

	if ( ch->mana < skill_table[gsn_sokatsui]->min_mana )
	{
	    send_to_char( "You can't channel enough spiritual pressure to execute your sokatsui.\n\r", ch );
	    return;
	}
	if (ch->focus < skill_table[gsn_sokatsui]->focus)
    {
		send_to_char( "You need to focus more.\n\r", ch );
		return;
    }
    else
    	ch->focus -= skill_table[gsn_sokatsui]->focus;

    WAIT_STATE( ch, skill_table[gsn_sokatsui]->beats );

    //sh_int z = get_aura(ch);

    if ( can_use_skill(ch, number_percent(),gsn_sokatsui ) )
    {
	dam = get_attmod(ch, victim) * number_range( 25, 60 );
	if (ch->charge > 0)
		dam = chargeDamMult(ch, dam);
	//to character
	act( AT_CYAN, "Ye lord! Mask of flesh and bone, flutter of wings, ye who bears the name of Man! ", ch, num_punct(dam), victim, TO_CHAR );
	act( AT_CYAN, "Truth and temperance, upon this sinless wall of dreams unleash but slightly the wrath of your claws.", ch, num_punct(dam), victim, TO_CHAR );
	act( AT_CYAN, "Hado number 33 Sokatsui. As you finish reciting the incantation a blue blast fires from your hand at $N. &W[$t]", ch, num_punct(dam), victim, TO_CHAR
);
	//to victim
	act( AT_CYAN, "$n recites Ye lord! Mask of flesh and bone, flutter of wings, ye who bears the name of Man!", ch, num_punct(dam), victim, TO_VICT );
	act( AT_CYAN, "Truth and temperance, upon this sinless wall of dreams unleash but slightly the wrath of your claws.", ch, num_punct(dam), victim, TO_VICT );
	act( AT_CYAN, "Hado number 33 Sokatsui. As they finish reciting the incantation a blue blast fires from their hand at you. &W[$t]", ch, num_punct(dam), victim,
TO_VICT );
	//to others in room
	act( AT_CYAN, "$n recites Ye lord! Mask of flesh and bone, flutter of wings, ye who bears the name of Man!", ch, num_punct(dam), victim, TO_NOTVICT );
	act( AT_CYAN, "Truth and temperance, upon this sinless wall of dreams unleash but slightly the wrath of your claws.", ch, num_punct(dam), victim, TO_NOTVICT );
	act( AT_CYAN, "Hado number 33 Sokatsui. As they finish reciting the incantation a blue blast fires from their hand at $N. &W[$t]", ch, num_punct(dam), victim,
TO_NOTVICT );
        dam = ki_absorb( victim, ch, dam, gsn_sokatsui );
	learn_from_success( ch, gsn_sokatsui );
	global_retcode = damage( ch, victim, dam, TYPE_HIT );
    }
    else
    {
	//miss to character
	act( AT_CYAN, "You missed $N with your sokatsui.", ch, NULL, victim, TO_CHAR );
	//miss victim
	act( AT_CYAN, "$n misses you with $s sokatsui.", ch, NULL, victim, TO_VICT );
	//miss others in room
	act( AT_CYAN, "$n missed $N with $s sokatsui.", ch, NULL, victim, TO_NOTVICT );
	learn_from_failure( ch, gsn_sokatsui );
	global_retcode = damage( ch, victim, 0, TYPE_HIT );
    }

    if( !is_android_h(ch) )
      ch->mana -= skill_table[gsn_sokatsui]->min_mana;
    return;
}

/* Shunpo (instant transmission)*/
void do_shunpo(CHAR_DATA *ch, char *argument )
{
    CHAR_DATA *victim;
	CHAR_DATA *fch;
	CHAR_DATA *fch_next;
	ROOM_INDEX_DATA *prev_room;

    if( !str_cmp(get_race(ch),"shinigami") )
    {
	if ( ch->mana < (skill_table[gsn_shunpo]->min_mana / 5) )
	{
	    send_to_char( "You don't have enough energy.\n\r", ch );
	    return;
	}
    }
    else
    {
        if ( ch->mana < skill_table[gsn_shunpo]->min_mana )
        {
            send_to_char( "You don't have enough energy.\n\r", ch );
            return;
        }
    }

    if( IS_NPC(ch) )
        return;

    if( ch->fighting )
    {
        send_to_char( "You cannot shunpo away from your enemie, that would be dishonorible.\n\r", ch );
        return;
    }

    if( xIS_SET(ch->in_room->room_flags, ROOM_ASTRALSHIELD) )
    {
	ch_printf(ch,"&RThis room is blocking your ability to focus your reiatsu and de-materialize.\n\r");
	return;
    }

    WAIT_STATE( ch, skill_table[gsn_shunpo]->beats );

    if ( (victim = get_char_ki_world(ch, argument)) == NULL
    ||   !can_astral(ch, victim)
	||   !in_hard_range(ch, victim->in_room->area)
	||   !in_soft_range(ch, victim->in_room->area)
	||	 xIS_SET(victim->in_room->room_flags, ROOM_PROTOTYPE )
	||   IS_IMMORTAL(victim))
    {
	    send_to_char( "You cannot locate their spirit ribbon.\n\r", ch );
            return;
    }

	if( is_split(victim) )
        {
            send_to_char( "You cannot loacate their spirit ribbon.\n\r", ch );
            return;
        }

	if ( !IS_NPC(victim) && !victim->desc )
	{
		send_to_char("You cannot shunpo to someone who has lost link.\n\r",ch);
		return;
	}

	if( !victim->in_room )
	{
		ch_printf(ch,"You disappear and reappear in the same spot.\n\r");
		return;
	}

	if( ch->in_room == victim->in_room )
	{
	  ch_printf(ch,"You're already there!\n\r");
	  return;
	}
	if( victim->master == ch )
	{
	  ch_printf(ch,"You can't shunpo to someone that is following you!\n\r");
	  return;
	}

if(!check_strain(ch)) return;
	if ( can_use_skill(ch, number_percent(), gsn_shunpo ) )
	{
	learn_from_success( ch, gsn_shunpo );
	act( AT_MAGIC, "Your body begins moving at speeds you can't comprehend, so you run toward your target!", ch, NULL, NULL, TO_CHAR );
	act( AT_MAGIC, "$n's vanishes in thin air. all you feel is a rush of air go by you!", ch, NULL, NULL, TO_ROOM );
    prev_room = ch->in_room;
    char_from_room( ch );
    if ( ch->mount )
    {
		char_from_room( ch->mount );
		char_to_room( ch->mount, victim->in_room );
    }
    char_to_room( ch, victim->in_room );
    for ( fch = prev_room->first_person; fch; fch = fch_next )
    {
	fch_next = fch->next_in_room;
	if ( ( fch != ch ) && fch->master && fch->master == ch )
        {
		char_from_room (fch);
		if( !is_leet(fch) )
		{
	    	  act( AT_ACTION, "You concentrate your reiatsu in to your legs!", fch, NULL, ch, TO_CHAR );
	    	  act( AT_ACTION, "Your body begins moving at remarkable speeds and you begin running toward your target!", fch, NULL, ch, TO_CHAR );
	    	  act( AT_ACTION, "$n's vanishes in thin air, and you feel a rush of wind go by you!", fch, NULL, ch, TO_ROOM );
		}
		else
		{
		 act( AT_ACTION, "You haxed a satellite! Omigawd u hax0r!",fch, NULL, ch, TO_CHAR );
               act( AT_ACTION, "$n's body dematerializes! OMGHAX!!!",fch, NULL, ch, TO_ROOM );
		}
		char_to_room( fch, victim->in_room );
		act( AT_MAGIC, "You run to your destination at super high speeds!", fch, NULL, ch, TO_CHAR );
	    	act( AT_ACTION, "You arrive at your destination and reclaim your reiatsu!", fch, NULL, ch, TO_CHAR );
		act( AT_MAGIC, "You feel a cold rush of wind behind you as $n appears there!", fch, NULL, ch, TO_ROOM );
		do_look( fch, "auto" );
        }
    }
    act( AT_MAGIC, "You feel a cold rush of wind behind you as $n appears there!", ch, NULL, NULL, TO_ROOM );
    do_look( ch, "auto" );
  }
  else
  {
    learn_from_failure( ch, gsn_shunpo );
    send_to_char( "&BYou cannot locate their spirit ribbon.\n\r", ch );
  }
  return;

}

void do_raikoho(CHAR_DATA *ch, char *argument ) /* Raikoho */
{
    CHAR_DATA *victim;
    int dam = 0;

    if( IS_NPC(ch) && is_split(ch) )
    {
        if( !ch->master )
          return;
        if( !can_use_skill( ch->master, number_percent(), gsn_raikoho ))
          return;
    }

    if ( IS_NPC(ch) && IS_AFFECTED( ch, AFF_CHARM ) )
    {
	send_to_char( "You can't concentrate enough for that.\n\r", ch );
	return;
    }

    if ( !IS_NPC(ch)
    &&   ch->exp < skill_table[gsn_raikoho]->skill_level[ch->class] )
    {
	send_to_char("You can't do that.\n\r", ch );
	return;
    }

    if ( ( victim = who_fighting( ch ) ) == NULL )
    {
	send_to_char( "You aren't fighting anyone.\n\r", ch );
	return;
    }

	if ( ch->mana < skill_table[gsn_raikoho]->min_mana )
	{
	    send_to_char( "You can't channel enough spiritual pressure to execute your raikoho.\n\r", ch );
	    return;
	}
	if (ch->focus < skill_table[gsn_raikoho]->focus)
    {
		send_to_char( "You need to focus more.\n\r", ch );
		return;
    }
    else
    	ch->focus -= skill_table[gsn_raikoho]->focus;

    WAIT_STATE( ch, skill_table[gsn_raikoho]->beats );

    //sh_int z = get_aura(ch);

    if ( can_use_skill(ch, number_percent(),gsn_raikoho ) )
    {
	dam = get_attmod(ch, victim) * number_range( 100, 163 );
	if (ch->charge > 0)
		dam = chargeDamMult(ch, dam);
	//to character
	act( AT_YELLOW, "Sprinkled on the bones of the beast! Sharp tower, red crystal, steel ring. Move ", ch, num_punct(dam), victim, TO_CHAR );
	act( AT_YELLOW, "and become the wind, stop and become the calm. The sound of warring spears fills", ch, num_punct(dam), victim, TO_CHAR );
	act( AT_YELLOW, "the empty castle! Hado number 63 Raikoho. As you finish reciting the incantation", ch, num_punct(dam), victim, TO_CHAR );
	act( AT_YELLOW, "a storm cloud appears over $N then lightining blasts down onto them  &W[$t]", ch, num_punct(dam), victim, TO_CHAR );
	//to victim
	act( AT_YELLOW, "$n recites 'Sprinkled on the bones of the beast! Sharp tower, red crystal, steel ring. Move and become the wind, stop and become the calm. The sound of warring spears fills the empty castle!", ch, num_punct(dam), victim, TO_VICT );

 // Removed Fixed act( AT_YELLOW, "the wind, stop and become the calm. The sound of warring spears fills the empty castle!", ch, num_punct(dam), victim, TO_VICT );

	act( AT_YELLOW, "Hado number 63 Raikoho.' As $n finishes reciting the incantation a storm cloud appears over you", ch, num_punct(dam), victim, TO_VICT );
	act( AT_YELLOW, "then lightining blasts down onto you  &W[$t]", ch, num_punct(dam), victim, TO_VICT );
	//to others in room
		act( AT_YELLOW, "$n recites 'Sprinkled on the bones of the beast! Sharp tower, red crystal, steel ring. Move and become ", ch, num_punct(dam), victim,
TO_NOTVICT );
	act( AT_YELLOW, "the wind, stop and become the calm. The sound of warring spears fills the empty castle!", ch, num_punct(dam), victim, TO_NOTVICT );
	act( AT_YELLOW, "Hado number 63 Raikoho.' As $n finishes reciting the incantation a storm cloud appears over $N", ch, num_punct(dam), victim, TO_NOTVICT );
	act( AT_YELLOW, "then lightining blasts down onto them  &W[$t]", ch, num_punct(dam), victim, TO_NOTVICT );
        dam = ki_absorb( victim, ch, dam, gsn_raikoho );
	learn_from_success( ch, gsn_raikoho );
	global_retcode = damage( ch, victim, dam, TYPE_HIT );
    }
    else
    {
	//miss to character
	act( AT_CYAN, "You missed $N with your raikoho.", ch, NULL, victim, TO_CHAR );
	//miss victim
	act( AT_CYAN, "$n misses you with $s raikoho.", ch, NULL, victim, TO_VICT );
	//miss others in room
	act( AT_CYAN, "$n missed $N with $s raikoho.", ch, NULL, victim, TO_NOTVICT );
	learn_from_failure( ch, gsn_raikoho );
	global_retcode = damage( ch, victim, 0, TYPE_HIT );
    }

    if( !is_android_h(ch) )
      ch->mana -= skill_table[gsn_raikoho]->min_mana;
    return;
}
