/****************************************************************************
 *			Dragonball: A New Dimension v1.1 Edition                *
 ****************************************************************************/

#include <sys/types.h>
#include <ctype.h>
#include <math.h>
#include <stdio.h>
#include <string.h>
#include <time.h>
#include "mud.h"

extern void transStatApply args((CHAR_DATA *ch, int strMod, int spdMod, int intMod, int conMod));
extern void transStatRemove args((CHAR_DATA *ch));

/*hollows place holder */

void do_masticate(CHAR_DATA *ch, char *argument )
{
    CHAR_DATA *victim;
    int dam = 0;

    if( IS_NPC(ch) && is_split(ch) )
    {
        if( !ch->master )
          return;
        if( !can_use_skill( ch->master, number_percent(), gsn_masticate ))
          return;
    }

    if ( IS_NPC(ch) && IS_AFFECTED( ch, AFF_CHARM ) )
    {
    send_to_char( "You can't concentrate enough for that.\n\r", ch );
    return;
    }

    if ( !IS_NPC(ch)
    &&   ch->exp < skill_table[gsn_masticate]->skill_level[ch->class] )
    {
    send_to_char( "You can't do that.\n\r", ch );
    return;
    }

    if ( ( victim = who_fighting( ch ) ) == NULL )
    {
    send_to_char( "You aren't fighting anyone.\n\r", ch );
    return;
    }

    if ( ch->mana < skill_table[gsn_masticate]->min_mana )
    {
        send_to_char( "You don't have enough energy.\n\r", ch );
        return;
    }
    if (ch->focus < skill_table[gsn_masticate]->focus)
    {
        send_to_char( "You need to focus more.\n\r", ch );
        return;
    }
    else
        ch->focus -= skill_table[gsn_masticate]->focus;

    WAIT_STATE( ch, skill_table[gsn_masticate]->beats );
    if ( can_use_skill(ch, number_percent(),gsn_masticate ) )
    {
    dam = get_attmod(ch, victim) * number_range( 15, 35 );
    if (ch->charge > 0)
        dam = chargeDamMult(ch, dam);
    act( AT_RED, "You growl viciously as you rush $N, jaws flying wide and clamping "
                  "down around $N's torse. Your eyes flash as they begin to bite and "
                  "chew against $N's flesh tightly, ripping their skin and devastating "
                  "their bones &W[$t]", ch, num_punct(dam), victim, TO_CHAR );
    act( AT_RED, "$n growls viciously as they rush you, jaws flying wide and clamping "
                 "down around your torse. $n's eyes flash as they begin to bite and  "
                 "chew against your flesh tightkt, ripping your skin and devastating "
                 "your bones &W[$t]", ch, num_punct(dam), victim, TO_VICT );
    act( AT_RED, "$n growls viciously as $s rushes at $N, jaws flying wide and clamping "
                 "down around $N's torso. $n's eyes flash as they begin to bite and chew "
                 "against $N's flesh tightly, ripping their skin and devastating their "
                 "bones &W[$t]", ch, num_punct(dam), victim, TO_NOTVICT );

    learn_from_success( ch, gsn_masticate );
    global_retcode = damage( ch, victim, dam, TYPE_HIT );
    }
    else
    {
    act( AT_RED, "Your jaws miss $N.", ch, NULL, victim, TO_CHAR );
    act( AT_RED, "$n misses you with a masticate attack.", ch, NULL, victim, TO_VICT );
    act( AT_RED, "$n missed $N with a Masticate.", ch, NULL, victim, TO_NOTVICT );
    learn_from_failure( ch, gsn_masticate );
    global_retcode = damage( ch, victim, 0, TYPE_HIT );
    }
    ch->mana -= skill_table[gsn_masticate]->min_mana;
    return;
}

void do_dark_leeches(CHAR_DATA *ch, char *argument )
{
    CHAR_DATA *victim;
    int dam = 0;

    if( IS_NPC(ch) && is_split(ch) )
    {
        if( !ch->master )
          return;
        if( !can_use_skill( ch->master, number_percent(), gsn_dleeches ))
          return;
    }

    if ( IS_NPC(ch) && IS_AFFECTED( ch, AFF_CHARM ) )
    {
    send_to_char( "You can't concentrate enough for that.\n\r", ch );
    return;
    }

    if ( !IS_NPC(ch)
    &&   ch->exp < skill_table[gsn_dleeches]->skill_level[ch->class] )
    {
    send_to_char( "You can't do that.\n\r", ch );
    return;
    }

    if ( ( victim = who_fighting( ch ) ) == NULL )
    {
    send_to_char( "You aren't fighting anyone.\n\r", ch );
    return;
    }

    if ( ch->mana < skill_table[gsn_dleeches]->min_mana )
    {
        send_to_char( "You don't have enough energy.\n\r", ch );
        return;
    }
    if (ch->focus < skill_table[gsn_dleeches]->focus)
    {
        send_to_char( "You need to focus more.\n\r", ch );
        return;
    }
    else
        ch->focus -= skill_table[gsn_dleeches]->focus;

    WAIT_STATE( ch, skill_table[gsn_dleeches]->beats );
    if ( can_use_skill(ch, number_percent(),gsn_dleeches ) )
    {
    dam = get_attmod(ch, victim) * number_range( 65, 75 );
    if (ch->charge > 0)
        dam = chargeDamMult(ch, dam);
    act( AT_DGREY, "You roar loudly and drop your head, shaking it wildly as several "
                   "leeches slide out into the air. The leeches fly towards $N latching "
                   "all over $N's body and sticking even as $N begins to scream and to "
                   "tug at them. You stick out your tongue at $N, a silent vibration "
                   "causing the leeches to explode one by one in a series over $N &W[$t]", ch, num_punct(dam), victim, TO_CHAR );
    act( AT_DGREY, "$n roars loudly and drops their head, shaking it wildly as several "
                   "leeches slide out into the air. The leeches fly towards you latching  "
                   "all over your body and sticking even as you scream and tug them. $n "
                   "sticks out their tongue at you, a silent vibration causing the leeches "
                   "to explode one by one in a series over you &W[$t]", ch, num_punct(dam), victim, TO_VICT );
    act( AT_DGREY, "$n roars loudly and drops their head, shaking it widly as several "
                   "leeches slide out into the air. The leeches fly towards $N, latching "
                   "all over $N's body and sticking even as $N screams and begins to tug "
                   "at them. $n sticks their tongue at $N, a silent vibration causing the "
                   "leeches to explode one by one in a series over $N &W[$t]", ch, num_punct(dam), victim, TO_NOTVICT );

    learn_from_success( ch, gsn_dleeches );
    global_retcode = damage( ch, victim, dam, TYPE_HIT );
    }
    else
    {
    act( AT_DGREY, "Your leeches miss $N.", ch, NULL, victim, TO_CHAR );
    act( AT_DGREY, "$n's leeches miss you.", ch, NULL, victim, TO_VICT );
    act( AT_DGREY, "$n's leeches missed $N.", ch, NULL, victim, TO_NOTVICT );
    learn_from_failure( ch, gsn_dleeches );
    global_retcode = damage( ch, victim, 0, TYPE_HIT );
    }
    ch->mana -= skill_table[gsn_dleeches]->min_mana;
    return;
}

void do_tongue_lash(CHAR_DATA *ch, char *argument )
{
    CHAR_DATA *victim;
    int dam = 0;

    if( IS_NPC(ch) && is_split(ch) )
    {
        if( !ch->master )
          return;
        if( !can_use_skill( ch->master, number_percent(), gsn_tongue_lash ))
          return;
    }

    if ( IS_NPC(ch) && IS_AFFECTED( ch, AFF_CHARM ) )
    {
    send_to_char( "You can't concentrate enough for that.\n\r", ch );
    return;
    }

    if ( !IS_NPC(ch)
    &&   ch->exp < skill_table[gsn_tongue_lash]->skill_level[ch->class] )
    {
    send_to_char( "You can't do that.\n\r", ch );
    return;
    }

    if ( ( victim = who_fighting( ch ) ) == NULL )
    {
    send_to_char( "You aren't fighting anyone.\n\r", ch );
    return;
    }

    if ( ch->mana < skill_table[gsn_tongue_lash]->min_mana )
    {
        send_to_char( "You don't have enough energy.\n\r", ch );
        return;
    }
    if (ch->focus < skill_table[gsn_tongue_lash]->focus)
    {
        send_to_char( "You need to focus more.\n\r", ch );
        return;
    }
    else
        ch->focus -= skill_table[gsn_tongue_lash]->focus;

    WAIT_STATE( ch, skill_table[gsn_tongue_lash]->beats );
    if ( can_use_skill(ch, number_percent(),gsn_tongue_lash ) )
    {
    dam = get_attmod(ch, victim) * number_range( 45, 58 );
    if (ch->charge > 0)
        dam = chargeDamMult(ch, dam);
    act( AT_DGREY, "You open your mouth slowly, a frightening cacophony of screams slipping "
                   "over your teeth. $N cluthces $s ears to try and keep the deafening sound "
                   "out. Your tongue darts out quickly, slashing $N a countless number of "
                   "times before it slips back. &W[$t]", ch, num_punct(dam), victim, TO_CHAR );
    act( AT_DGREY, "$n opens their mouth slowly, a frightening cacophony of screams slipping "
                   "over their teeth. You clutch your ears to try and keep the deafening sound"
                   "out. $n's tongue darts out quickly, slashing you a countless number of "
                   "times before it slips back. &W[$t]", ch, num_punct(dam), victim, TO_VICT );
    act( AT_DGREY, "$n opens their mouth slowly, a frightening cacophony of screams slipping "
                   "over their teeth. $N clutches their ears to try and keep the defeaning sound "
                   "out. $n's tongue darts out quickly, slashing $N a countless number of times "
                   "before it slips back. &W[$t]", ch, num_punct(dam), victim, TO_NOTVICT );

    learn_from_success( ch, gsn_tongue_lash );
    global_retcode = damage( ch, victim, dam, TYPE_HIT );
    }
    else
    {
    act( AT_DGREY, "Your tongue miss $N.", ch, NULL, victim, TO_CHAR );
    act( AT_DGREY, "$n's tongue misses you.", ch, NULL, victim, TO_VICT );
    act( AT_DGREY, "$n's tongue missed $N.", ch, NULL, victim, TO_NOTVICT );
    learn_from_failure( ch, gsn_tongue_lash );
    global_retcode = damage( ch, victim, 0, TYPE_HIT );
    }
    ch->mana -= skill_table[gsn_tongue_lash]->min_mana;
    return;
}

void do_cero(CHAR_DATA *ch, char *argument )
{
    CHAR_DATA *victim;
    int dam = 0;

    if( IS_NPC(ch) && is_split(ch) )
    {
        if( !ch->master )
          return;
        if( !can_use_skill( ch->master, number_percent(), gsn_cero ))
          return;
    }

    if ( IS_NPC(ch) && IS_AFFECTED( ch, AFF_CHARM ) )
    {
    send_to_char( "You can't concentrate enough for that.\n\r", ch );
    return;
    }

    if ( !IS_NPC(ch)
    &&   ch->exp < skill_table[gsn_cero]->skill_level[ch->class] )
    {
    send_to_char( "You can't do that.\n\r", ch );
    return;
    }

    if ( ( victim = who_fighting( ch ) ) == NULL )
    {
    send_to_char( "You aren't fighting anyone.\n\r", ch );
    return;
    }

    if ( ch->mana < skill_table[gsn_cero]->min_mana )
    {
        send_to_char( "You don't have enough energy.\n\r", ch );
        return;
    }
    if (ch->focus < skill_table[gsn_cero]->focus)
    {
        send_to_char( "You need to focus more.\n\r", ch );
        return;
    }
    else
        ch->focus -= skill_table[gsn_cero]->focus;

    WAIT_STATE( ch, skill_table[gsn_cero]->beats );
    if ( can_use_skill(ch, number_percent(),gsn_cero ) )
    {
    dam = get_attmod(ch, victim) * number_range( 50, 68 );
    if (ch->charge > 0)
        dam = chargeDamMult(ch, dam);
    act( AT_RED, "You growl loudly, throwing your head back as a blood red energy flows into "
                 "your mouth from every conceivable direction. You're eyes brighten before "
                 "you swing your head down, the bright energy disappearing for just a moment. "
                 "You roar as a thunderous blast explodes from inside your throat, nearly "
                 "shattering their mask as the wave envelopes $N. &W[$t]", ch, num_punct(dam), victim, TO_CHAR );
    act( AT_RED, "$n growls loudly, throwing their head back as blood red energy flows into "
                 "$n's mouth from every conceivable direction. $n's eyes brighten before $n "
                 "swings their head down, the bright energy dissapearing for just a moment "
                 "$n roars as a thunderous blast explodes from inside $n's throat, nearly "
                 "shattering their mask as the wave envelopes you. &W[$t]", ch, num_punct(dam), victim, TO_VICT );
    act( AT_RED, "$n growls loudly, throwing their head back as a blood red energy flows into "
                 "$n's mouth from every conceivable direction. $n's eyes brighten before $n "
                 "swings their head down, the bright energy dissapearing for just a moment. $n "
                 "roars as a thunderous blast explodes from inside $n's throat, nearly shattering "
                 "their mask as the wave envelopes $N. &W[$t]", ch, num_punct(dam), victim, TO_NOTVICT );

    learn_from_success( ch, gsn_cero );
    global_retcode = damage( ch, victim, dam, TYPE_HIT );
    }
    else
    {
    act( AT_RED, "Your cero misses $N.", ch, NULL, victim, TO_CHAR );
    act( AT_RED, "$n's cero misses you.", ch, NULL, victim, TO_VICT );
    act( AT_RED, "$n's cero missed $N.", ch, NULL, victim, TO_NOTVICT );
    learn_from_failure( ch, gsn_cero );
    global_retcode = damage( ch, victim, 0, TYPE_HIT );
    }
    ch->mana -= skill_table[gsn_cero]->min_mana;
    return;
}

void do_miasma_breath(CHAR_DATA *ch, char *argument )
{
    CHAR_DATA *victim;
    int dam = 0;

    if( IS_NPC(ch) && is_split(ch) )
    {
        if( !ch->master )
          return;
        if( !can_use_skill( ch->master, number_percent(), gsn_mbreath ))
          return;
    }

    if ( IS_NPC(ch) && IS_AFFECTED( ch, AFF_CHARM ) )
    {
    send_to_char( "You can't concentrate enough for that.\n\r", ch );
    return;
    }

    if ( !IS_NPC(ch)
    &&   ch->exp < skill_table[gsn_mbreath]->skill_level[ch->class] )
    {
    send_to_char( "You can't do that.\n\r", ch );
    return;
    }

    if ( ( victim = who_fighting( ch ) ) == NULL )
    {
    send_to_char( "You aren't fighting anyone.\n\r", ch );
    return;
    }

    if ( ch->mana < skill_table[gsn_mbreath]->min_mana )
    {
        send_to_char( "You don't have enough energy.\n\r", ch );
        return;
    }
    if (ch->focus < skill_table[gsn_mbreath]->focus)
    {
        send_to_char( "You need to focus more.\n\r", ch );
        return;
    }
    else
        ch->focus -= skill_table[gsn_mbreath]->focus;

    WAIT_STATE( ch, skill_table[gsn_mbreath]->beats );
    if ( can_use_skill(ch, number_percent(),gsn_mbreath ) )
    {
    dam = get_attmod(ch, victim) * number_range( 70, 79 );
    if (ch->charge > 0)
        dam = chargeDamMult(ch, dam);
    act( AT_YELLOW, "You take a very deep breath, breathing in so slowly for so long that "
                    "your chest begins to bulge. Your eyes flash as your lungs finally fill, "
                    "leaning forward quickly. A disgusting exhale follows as your mouth flies "
                    "open, surrounding $N in a black cloud while it chokes, the cloud so "
                    "noxious it burns $N'skin as well. &W[$t]", ch, num_punct(dam), victim, TO_CHAR );
    act( AT_YELLOW, "$n takes a very deep breath, breathing in so slowly for so long that "
                    "their chest begins to bulge. $n's eyes flash as their lungs finally fill, "
                    "leaning forward quickly. A disgusting exhale follows as $n's mouth flies "
                    "open, surrounding $N in a black cloud while it chokes, the cloud so noxious "
                    "it burns $N's skin as well. &W[$t]", ch, num_punct(dam), victim, TO_VICT );
    act( AT_YELLOW, "$n takes a very deep breath, breathing in so slowly for so long that their "
                    "chest begins to bulge. $n's eyes flash as their lungs finally fill, leaning "
                    "forward quickly. A disgusting exhale follows as $n's mouth flies open, "
                    "surrounding $N in a black cloud while it chokes, the cloud so noxious it "
                    "burns $N's skin as well. &W[$t]", ch, num_punct(dam), victim, TO_NOTVICT );

    learn_from_success( ch, gsn_mbreath );
    global_retcode = damage( ch, victim, dam, TYPE_HIT );
    }
    else
    {
    act( AT_YELLOW, "Your breath misses $N.", ch, NULL, victim, TO_CHAR );
    act( AT_YELLOW, "$n's breath misses you.", ch, NULL, victim, TO_VICT );
    act( AT_YELLOW, "$n's breath missed $N.", ch, NULL, victim, TO_NOTVICT );
    learn_from_failure( ch, gsn_mbreath );
    global_retcode = damage( ch, victim, 0, TYPE_HIT );
    }
    ch->mana -= skill_table[gsn_mbreath]->min_mana;
    return;
}


void do_hidden_tendrils(CHAR_DATA *ch, char *argument )
{
    CHAR_DATA *victim;
    int dam = 0;

    if( IS_NPC(ch) && is_split(ch) )
    {
        if( !ch->master )
          return;
        if( !can_use_skill( ch->master, number_percent(), gsn_htendrils ))
          return;
    }

    if ( IS_NPC(ch) && IS_AFFECTED( ch, AFF_CHARM ) )
    {
    send_to_char( "You can't concentrate enough for that.\n\r", ch );
    return;
    }

    if ( !IS_NPC(ch)
    &&   ch->exp < skill_table[gsn_htendrils]->skill_level[ch->class] )
    {
    send_to_char( "You can't do that.\n\r", ch );
    return;
    }

    if ( ( victim = who_fighting( ch ) ) == NULL )
    {
    send_to_char( "You aren't fighting anyone.\n\r", ch );
    return;
    }

    if ( ch->mana < skill_table[gsn_htendrils]->min_mana )
    {
        send_to_char( "You don't have enough energy.\n\r", ch );
        return;
    }
    if (ch->focus < skill_table[gsn_htendrils]->focus)
    {
        send_to_char( "You need to focus more.\n\r", ch );
        return;
    }
    else
        ch->focus -= skill_table[gsn_htendrils]->focus;

    WAIT_STATE( ch, skill_table[gsn_htendrils]->beats );
    if ( can_use_skill(ch, number_percent(),gsn_htendrils ) )
    {
    dam = get_attmod(ch, victim) * number_range( 75, 95 );
    if (ch->charge > 0)
        dam = chargeDamMult(ch, dam);
    act( AT_ORANGE, "You swing your head around once, jerking it towards $N violently. Your "
                    "hair spreads out from the sudden shake, the thick strands jetting towards "
                    "$N. $N gasps in shock as $N's hair easily pierces $N through its chest &W[$t]", ch, num_punct(dam), victim, TO_CHAR );
    act( AT_ORANGE, "$n swings their head around once, jerking it towards you violently.$n's "
                    "hair spreads out from the sudden shake, the thick strands getting towards "
                    "you. You gasp in shock as $n's hair easily pierces through your chest. &W[$t]", ch, num_punct(dam), victim, TO_VICT );
    act( AT_ORANGE, "$n swings their head around once, jerking it towards $N violently. $n's "
                    "hair spreads out from the sudden shake, the thick strands jetting towards "
                    "$N. $N gasps in shock as $n's hair easily pierces $N thought its chest. &W[$t]", ch, num_punct(dam), victim, TO_NOTVICT );

    learn_from_success( ch, gsn_htendrils );
    global_retcode = damage( ch, victim, dam, TYPE_HIT );
    }
    else
    {
    act( AT_ORANGE, "Your hair misses $N.", ch, NULL, victim, TO_CHAR );
    act( AT_ORANGE, "$n's hair misses you.", ch, NULL, victim, TO_VICT );
    act( AT_ORANGE, "$n's hair missed $N.", ch, NULL, victim, TO_NOTVICT );
    learn_from_failure( ch, gsn_htendrils );
    global_retcode = damage( ch, victim, 0, TYPE_HIT );
    }
    ch->mana -= skill_table[gsn_htendrils]->min_mana;
    return;
}

void do_talon_spear(CHAR_DATA *ch, char *argument )
{
    CHAR_DATA *victim;
    int dam = 0;

    if( IS_NPC(ch) && is_split(ch) )
    {
        if( !ch->master )
          return;
        if( !can_use_skill( ch->master, number_percent(), gsn_tspear ))
          return;
    }

    if ( IS_NPC(ch) && IS_AFFECTED( ch, AFF_CHARM ) )
    {
    send_to_char( "You can't concentrate enough for that.\n\r", ch );
    return;
    }

    if ( !IS_NPC(ch)
    &&   ch->exp < skill_table[gsn_tspear]->skill_level[ch->class] )
    {
    send_to_char( "You can't do that.\n\r", ch );
    return;
    }

    if ( ( victim = who_fighting( ch ) ) == NULL )
    {
    send_to_char( "You aren't fighting anyone.\n\r", ch );
    return;
    }

    if ( ch->mana < skill_table[gsn_tspear]->min_mana )
    {
        send_to_char( "You don't have enough energy.\n\r", ch );
        return;
    }
    if (ch->focus < skill_table[gsn_tspear]->focus)
    {
        send_to_char( "You need to focus more.\n\r", ch );
        return;
    }
    else
        ch->focus -= skill_table[gsn_tspear]->focus;

    WAIT_STATE( ch, skill_table[gsn_tspear]->beats );
    if ( can_use_skill(ch, number_percent(),gsn_tspear ) )
    {
    dam = get_attmod(ch, victim) * number_range( 108, 135 );
    if (ch->charge > 0)
        dam = chargeDamMult(ch, dam);
    act( AT_GREEN, "You point your claw at $N, all fingers together before they spread wide. Your "
                   "talons suddenly strike out at $N, piercing through its shoulders. $N screams out "
                   "in pain before the other three talons pierce $N's chest neck then forehead. &W[$t]", ch, num_punct(dam), victim, TO_CHAR );
    act( AT_GREEN, "$n points their claw at you, all fingers together before they spread wide. $n's "
                   "talons suddenly strike out at you,  piercing through your shoulders. You scream out "
                   "in pain before the other three talons pierce your chest, neck then forehead. &W[$t]", ch, num_punct(dam), victim, TO_VICT );
    act( AT_GREEN, "$n points their claw at $N, all fingers together before they spread wide. $n's talons "
                    "sudenly strike out at $N, piercing through $s shoulders. $N screams out in pain before "
                   "the other three talons pierce $N's chest, neck then forehead. &W[$t]", ch, num_punct(dam), victim, TO_NOTVICT );

    learn_from_success( ch, gsn_tspear );
    global_retcode = damage( ch, victim, dam, TYPE_HIT );
    }
    else
    {
    act( AT_GREEN, "Your talon misses $N.", ch, NULL, victim, TO_CHAR );
    act( AT_GREEN, "$n's talon misses you.", ch, NULL, victim, TO_VICT );
    act( AT_GREEN, "$n's talon missed $N.", ch, NULL, victim, TO_NOTVICT );
    learn_from_failure( ch, gsn_tspear );
    global_retcode = damage( ch, victim, 0, TYPE_HIT );
    }
    ch->mana -= skill_table[gsn_tspear]->min_mana;
    return;
}


void do_sorrow(CHAR_DATA *ch, char *argument)
{
    if( IS_NPC(ch) && !is_split(ch) )
	return;

    if( IS_NPC(ch) && is_split(ch) )
    {
        if( !ch->master )
          return;
        if( !can_use_skill( ch->master, number_percent(), gsn_sorrow))
          return;
    }

    if( !IS_NPC(ch) )
    {
      if( IS_SET(ch->pcdata->flags, PCFLAG_KNOWSMYSTIC) )
      {
        ch_printf(ch,"You are unable to call upon those powers while you know mystic.\n\r");
        return;
      }
    }

    if( wearing_chip(ch) )
    {
        ch_printf(ch,"You can't while you have a chip installed.\n\r");
        return;
    }

    if( wearing_sentient_chip(ch) )
    {
	ch_printf(ch,"You can't use this.");
	return;
    }

	if (xIS_SET((ch)->affected_by, AFF_SORROW))
	{
		send_to_char("You power down and return to normal.\n\r", ch);
		xREMOVE_BIT((ch)->affected_by, AFF_SORROW);
		ch->pl = ch->exp;
		transStatRemove(ch);
		return;
	}

	if (ch->mana < skill_table[gsn_sorrow]->min_mana)
	{
	    send_to_char("You don't have enough energy.\n\r", ch);
	    return;
	}

	sh_int z = get_aura(ch);

	WAIT_STATE(ch, skill_table[gsn_sorrow]->beats);
	if (can_use_skill(ch, number_percent(), gsn_sorrow) )
	{
		/* note to self in case this needs to be changed
		   back to white color. it originally says "bright
		   white light" in a section of these messages.
		   Changed it to bright light for now. - Karma */
		act(z, "You let out a blood curdling scream, clutching your chest while a dark aura slowly forms. All sound around you gets sucked out of the air, a strange darkness enveloping your entire body quickly. Your muscles bulge grotesquely as your form pulses, makng them grow several times larger", ch, NULL, NULL, TO_CHAR);
		act(z, "$n lets out a blood curdling scream, clutching their chest while a dark aura slowly forms. All sound around $n gets sucked out of the air, a strange darkness enveloping their entire body quickly. $n muscles buldge grotesquely as their form pulses, making them grow several times larger", ch, NULL, NULL, TO_NOTVICT);
		xSET_BIT((ch)->affected_by, AFF_SORROW);
		ch->pl = ch->exp * 10;
		transStatApply(ch, 12, 12, 6, 25);
		learn_from_success(ch, gsn_sorrow);
	}

	else
	{
		act( z, "You cant seem to gather enough evil energy to transform.", ch, NULL, NULL, TO_CHAR );
		act( z, "$n cant seem to get enough evil energy to transform.", ch, NULL, NULL, TO_NOTVICT );
		learn_from_failure( ch, gsn_sorrow );
	}

	ch->mana -= skill_table[gsn_sorrow]->min_mana;
	return;
}

void do_envy(CHAR_DATA *ch, char *argument)
{
    if( IS_NPC(ch) && !is_split(ch) )
	return;

    if( IS_NPC(ch) && is_split(ch) )
    {
        if( !ch->master )
          return;
        if( !can_use_skill( ch->master, number_percent(), gsn_envy))
          return;
    }

    if( !IS_NPC(ch) )
    {
      if( IS_SET(ch->pcdata->flags, PCFLAG_KNOWSMYSTIC) )
      {
        ch_printf(ch,"You are unable to call upon those powers while you know mystic.\n\r");
        return;
      }
    }

    if( wearing_chip(ch) )
    {
        ch_printf(ch,"You can't while you have a chip installed.\n\r");
        return;
    }

    if( wearing_sentient_chip(ch) )
    {
	ch_printf(ch,"You can't use this.");
	return;
    }

	if (xIS_SET((ch)->affected_by, AFF_ENVY))
	{
		send_to_char("You power down and return to normal.\n\r", ch);
		xREMOVE_BIT((ch)->affected_by, AFF_ENVY);
		ch->pl = ch->exp;
		transStatRemove(ch);
		return;
	}

	if (ch->mana < skill_table[gsn_envy]->min_mana)
	{
	    send_to_char("You don't have enough energy.\n\r", ch);
	    return;
	}

	sh_int z = get_aura(ch);

	WAIT_STATE(ch, skill_table[gsn_envy]->beats);
	if (can_use_skill(ch, number_percent(), gsn_envy) )
	{
		/* note to self in case this needs to be changed
		   back to white color. it originally says "bright
		   white light" in a section of these messages.
		   Changed it to bright light for now. - Karma */
		act(z, "You tear at your mask, eyes flaring a bright gold. You roar so loudly that the ground begins to crack and crumble, causing debris to fly out in every direction. Your body surges as your aura becomes even darker all around than before you rocket up becoming a monstrous size.", ch, NULL, NULL, TO_CHAR);
		act(z, "$n tears at its's mask, eyes flaring a bright gold. $n roars so loudly that the ground begins to cracke and crumble, causing decris to fly out in every direction. $n's body surges as their aura becomes even darker all around them before $n rockets up, becoming a monstrous size", ch, NULL, NULL, TO_NOTVICT);
		xSET_BIT((ch)->affected_by, AFF_ENVY);
		ch->pl = ch->exp * 18;
		transStatApply(ch, 22, 22, 16, 30);
		learn_from_success(ch, gsn_envy);
	}

	else
	{
		act( z, "Your body begins to increase in size but then reverts back to normal.", ch, NULL, NULL, TO_CHAR );
		act( z, "$n's body begins to grow in size but then reverts back to normal.", ch, NULL, NULL, TO_NOTVICT );
		learn_from_failure( ch, gsn_envy );
	}

	ch->mana -= skill_table[gsn_envy]->min_mana;
	return;
}


void do_rage(CHAR_DATA *ch, char *argument)
{
    if( IS_NPC(ch) && !is_split(ch) )
	return;

    if( IS_NPC(ch) && is_split(ch) )
    {
        if( !ch->master )
          return;
        if( !can_use_skill( ch->master, number_percent(), gsn_rage))
          return;
    }

    if( !IS_NPC(ch) )
    {
      if( IS_SET(ch->pcdata->flags, PCFLAG_KNOWSMYSTIC) )
      {
        ch_printf(ch,"You are unable to call upon those powers while you know mystic.\n\r");
        return;
      }
    }

    if( wearing_chip(ch) )
    {
        ch_printf(ch,"You can't while you have a chip installed.\n\r");
        return;
    }

    if( wearing_sentient_chip(ch) )
    {
	ch_printf(ch,"You can't use this.");
	return;
    }

	if (xIS_SET((ch)->affected_by, AFF_RAGE))
	{
		send_to_char("You power down and return to normal.\n\r", ch);
		xREMOVE_BIT((ch)->affected_by, AFF_RAGE);
		ch->pl = ch->exp;
		transStatRemove(ch);
		return;
	}

	if (ch->mana < skill_table[gsn_rage]->min_mana)
	{
	    send_to_char("You don't have enough energy.\n\r", ch);
	    return;
	}

	sh_int z = get_aura(ch);

	WAIT_STATE(ch, skill_table[gsn_rage]->beats);
	if (can_use_skill(ch, number_percent(), gsn_rage) )
	{
		/* note to self in case this needs to be changed
		   back to white color. it originally says "bright
		   white light" in a section of these messages.
		   Changed it to bright light for now. - Karma */
		act(z, "You claw at your mask and body in a fury, eyes shinning a blazing crimson. You fall into all hous as your form becomes covered in midnight black fur. You dig into the dirt with a deafening scream, your claws becoming gigantic and your aura and inferno of shadow.", ch, NULL, NULL, TO_CHAR);
		act(z, "$n claws at it's mask and body in a fury, eyes shining a blazing crimson. $n falls onto all fours as its form becomes covered in midnight black fur. $n digs into the dirt with a defeaning scream, its claws becoming gigantic and its aura an inferno of shadow.", ch, NULL, NULL, TO_NOTVICT);
		xSET_BIT((ch)->affected_by, AFF_RAGE);
		ch->pl = ch->exp * 27;
		transStatApply(ch, 27, 27, 27, 35);
		learn_from_success(ch, gsn_rage);
	}

	else
	{
		act( z, "Your aura thickes but then goes back to normal.", ch, NULL, NULL, TO_CHAR );
		act( z, "$n's aura thickes but then goes back to normal.", ch, NULL, NULL, TO_NOTVICT );
		learn_from_failure( ch, gsn_rage );
	}

	ch->mana -= skill_table[gsn_rage]->min_mana;
	return;
}

