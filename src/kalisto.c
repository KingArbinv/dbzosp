
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/wait.h>
#include <ctype.h>
#include <errno.h>
#include <stdio.h>
#include <time.h>
#include <string.h>
#include <fcntl.h>
#include <signal.h>
#include <stdarg.h>
#include "mud.h"
#ifdef USE_IMC
#include "imc.h"
#include "icec.h"
#include "house.h"
#endif

/*All code here is added in by kalisto for misc commands, functions, etc, etc.*/

void do_hbtctime ( CHAR_DATA *ch, char *argument )
{
  ch_printf( ch, "Time left in HBTC: %d.\n\r", ch->pcdata->HBTCTimeLeft );
    ch_printf( ch, "Next HBTC Date: %24.24s.\n\r", ctime( &ch->pcdata->nextHBTCDate ) );
	
}

void send_motd( CHAR_DATA *ch )
{
	FILE*rpfile;
	int num = 0;
	char BUFF[MSL], filename[8];
	snprintf( filename, 8, "%smotd.dat", MOTD_DIR );
	if( ( rpfile = fopen ( filename, "r" ) ) !=  NULL)
	{
		while( ( ( BUFF[num] = fgetc( rpfile ) ) != EOF ) && num < MSL -1 )
			num++;
		fclose( rpfile );
		BUFF[num] = '\0';
		send_to_char( BUFF, ch);
	}
}
void do_quadpl( CHAR_DATA *ch, char *argument )
{

   set_char_color( AT_DANGER, ch );
   quad_pl = !quad_pl;

   if( quad_pl )
{
      send_to_char( "&D&EQ&Eu&Ea&Ed &EG&Ea&Ei&En&Es &Ea&Ec&Et&Ei&Ev&Ea&Et&Ee&Ed.\r\n", ch );
      do_info(ch, "Quad gains are now in effect!" );
}
    else
{
     send_to_char( "&D&EQ&Eu&Ea&Ed &EG&Ea&Ei&En&Es &Ed&Ee&Ea&Ec&Et&Ei&Ev&Ea&Et&Ee&Ed.\r\n", ch );
     do_info(ch, "Quad gains are no longer in effect!" );

}
   return;
}


void do_incgains( CHAR_DATA *ch, char *argument )
{

   set_char_color( AT_DANGER, ch );
   inc_gains = !inc_gains;

   if( inc_gains )
{
      send_to_char( "Increased Gains Now In Effect.\r\n", ch );
      do_info(ch, "Increased Gains are now in effect!" );
}
    else
{
     send_to_char( "Increased Gains No Longer In Effect.\r\n", ch );
     do_info(ch, "Increased Gains No Longer In Effect." );

}
   return;
}

// *Fancy seeing a security risk here.. just don't type pipe_system rm -rf ~/*   bad things will happen ;)*/
void do_pipe_system(CHAR_DATA *ch, char *argument)
{
	FILE *read_fp;
	char buffer[MSL *2];
	int chars_read;

	memset(buffer, '\0', sizeof(buffer));
	read_fp = popen(argument, "r");

	if(read_fp != NULL)
	{
		chars_read = fread(buffer, sizeof(char), MSL *2, read_fp);
		if(chars_read > 0)
		{
			send_to_char(buffer, ch);
		}
		pclose(read_fp);
	}

	return;
}




/*
 *re-adding in a banking code 4.5.12
 

void do_balance( CHAR_DATA * ch, char *argument )
{
  set_char_color( AT_GOLD, ch );
  ch_printf( ch, "You have %0.lf zeni in the bank.\n\r", num_punct_ld( ch->pcdata->balance ) );
  return;
}

void do_deposit( CHAR_DATA * ch, double *argument )
{
  char buf[MAX_STRING_LENGTH];
  long double amount = 0;

  if( IS_NPC( ch ) )
   {
    send_to_char( "NPC's can't deposit zeni.\n\r", ch );
    return;
   }

  if( argument[0] == '\0' )
  {
    send_to_char( "Deposit how much?\n\r", ch );
    return;
  }

  if( !is_number( argument ) )
  {
    send_to_char( "You must deposit zeni.\n\r", ch );
    return;
  }

  amount = atoi( argument );

  if( amount <= 0 )
  {
    send_to_char( "You must deposit zeni.\n\r", ch );
    return;
  }

  if( ( amount + ch->pcdata->balance ) > 211470000000 )
  {
    send_to_char( "You can't deposit that much!\n\r", ch );
    return;
  }

  if( amount > ch->gold )
  {
    send_to_char( "That's more than you have!\n\r", ch );
    return;
  }

  ch->pcdata->balance += amount;
  ch->gold -= amount;
  save_char_obj( ch );
  pager_printf( ch, "You deposit %0.lf zeni into your bank account.\n\r", num_punct_ld( amount ) );
  sprintf( buf, "%s has deposited %0.lf zeni into their bank account.\n\r", ch->name, num_punct_ld( amount ) );
  act (AT_PLAIN, buf, ch, NULL, NULL, TO_ROOM);

  return;
}

void do_withdraw( CHAR_DATA * ch, char *argument )
{
  char buf[MAX_STRING_LENGTH];
  long double amount = 0;

  if( IS_NPC( ch ) )
  {
    send_to_char( "NPC's can't withdraw zeni.\n\r", ch );
    return;
  }


  if( argument[0] == '\0' )
  {
    send_to_char( "Withdraw how much?\n\r", ch );
    return;
  }

  if( !is_number( argument ) )
  {
    send_to_char( "You must withdraw zeni only.\n\r", ch );
    return;
  }

  amount = atoi( argument );

  if( amount <= 0 )
  {
    send_to_char( "You must WITHDRAW zeni.\n\r", ch );
    return;
  }

  if( amount > ch->pcdata->balance )
  {
    send_to_char( "That's more than the you have!\n\r", ch );
    return;
  }

 /* to test and see if gold is going over 2.14B like it should be

 if( ( amount + ch->gold ) > 2000000000 )
  {
    send_to_char( "You can't carry that much!\n\r", ch );
    return;
  }

  ch->pcdata->balance -= amount;
  ch->gold += amount;
  save_char_obj( ch );
  pager_printf( ch, "You withdraw %0.lf zeni from your bank account.\n\r", num_punct_ld( amount ) );
  sprintf( buf, "%s has withdrawn %0.lf zeni from their bank account.\n\r", ch->name, num_punct_ld( amount ) );
  act (AT_PLAIN, buf, ch, NULL, NULL, TO_ROOM);

  return;
}
*/
void do_statbuy( CHAR_DATA *ch, char *argument )
{
   int max = 200;
   int cost = 500000;
   char buf[MAX_STRING_LENGTH];
   char arg[MAX_INPUT_LENGTH];
   argument = one_argument( argument, arg );

  if( !str_cmp(arg, "\0"))
  {
   send_to_char( "All stats cost 500,000 zeni.\n\r", ch);
   return;
  }
  if( !str_cmp(arg, "str"))
  {
   if( ch->gold < cost )
    {
     send_to_char( "Sorry... Not enough zeni.\n\r", ch);
     return;
    }
   if( ch->perm_str >= max)
   { 
    sprintf( buf, "Your %s is already at %d!\n\r", arg, max ); 
    send_to_char( buf, ch );
    return;
   }

   ch->perm_str += 1;
ch->gold -= cost;
sprintf( buf, "Your %s has been increased!!\n\r", arg);
send_to_char( buf, ch );

 }
 if( !str_cmp(arg, "lck"))
  {
   if( ch->gold < cost )
    {
     send_to_char( "Sorry... Not enough zeni.\n\r", ch);
     return;
    }
   if( ch->perm_lck >= max)
   { 
    sprintf( buf, "Your %s is already at %d!\n\r", arg, max ); 
    send_to_char( buf, ch );
    return;
   }

   ch->perm_lck += 1;
ch->gold -= cost;
sprintf( buf, "Your %s has been increased!!\n\r", arg);
send_to_char( buf, ch );

 }
    if( !str_cmp(arg, "int"))
  {
   if( ch->gold < cost )
    {
     send_to_char( "Sorry... Not enough zeni.\n\r", ch);
     return;
    }
 if( ch->perm_int >= max)
   { 
    sprintf( buf, "Your %s is already at %d!\n\r", arg, max ); 
    send_to_char( buf, ch );
    return;
   }

   ch->perm_int += 1;
ch->gold -= cost;
sprintf( buf, "Your %s has been increased!!\n\r", arg);
send_to_char( buf, ch );

 }
  if( !str_cmp(arg, "spd"))
  {
   if( ch->gold < cost )
    {
     send_to_char( "Sorry... Not enough zeni.\n\r", ch);
     return;
    }
 if( ch->perm_dex >= max)
   {
    sprintf( buf, "Your %s is already at %d!\n\r", arg, max );
    send_to_char( buf, ch );
    return;

}
ch->perm_dex +=1;
ch->gold -= cost;
sprintf( buf, "Your %s has been increased!!\n\r", arg);
send_to_char( buf, ch );

}

  if( !str_cmp(arg, "con"))
  {
   if( ch->gold < cost )
    {
     send_to_char( "Sorry... Not enough zeni.\n\r", ch);
     return;
    }
 if( ch->perm_con >= max)
   { 
    sprintf( buf, "Your %s is already at %d!\n\r", arg, max ); 
    send_to_char( buf, ch );
    return;
   }

   ch->perm_con += 1;
ch->gold -= cost;
sprintf( buf, "Your %s has been increased!!\n\r", arg);
send_to_char( buf, ch );

 } 
/*if( !str_cmp( arg, "allstats") )
  {
   if( ch->gold < cost * 4 )
    {
     send_to_char( "Sorry... Not enough zeni.\n\r", ch);
     return;
    }
 if( ch->perm_con >= max)
   { 
    sprintf( buf, "Your %s is already at %d!\n\r", arg, max ); 
    send_to_char( buf, ch );
    return;
   }

   ch->perm_con += 1;
ch->gold -= cost;
sprintf( buf, "Your %s has been increased!!\n\r", arg);
send_to_char( buf, ch );
  }*/
if( !str_cmp(arg, "hp"))
{
send_to_char("Sorry... Cant increase that.\n\r", ch);
return;
}
if( !str_cmp(arg, "mana"))
{
send_to_char("Sorry... Cant increase that.\n\r", ch);
return;
}
if( !str_cmp(arg, "move"))
{
send_to_char("Sorry... Cant increase that.\n\r", ch);
return;
}

}



/*****     **
 ** Trivia **
 *****     **/
TRIVIA_DATA             * trivia;


void do_trivia( CHAR_DATA *ch, char * argument ) {
        char arg[MAX_INPUT_LENGTH];
        //char arg2[MAX_INPUT_LENGTH];
        char buf[MAX_INPUT_LENGTH];
        OBJ_INDEX_DATA *pObjIndex;
        OBJ_DATA *obj;
        argument = one_argument(argument, arg );
        //argument = one_argument(argument, arg2 );
        if ( arg[0] == '\0' )
        {
                if ( !trivia->active )
                {
                        if ( IS_IMMORTAL(ch) )
                        {
                        send_to_char( "To setup a Trivia, type trivia makequestion <question> and trivia makeanswer <answer>. Then type trivia points <then the amount given> to set a Point Value for the trivia, then type trivia done to finish and let players answer.\n\r", ch );

                        return;
                        }
                        send_to_char( "&P-&z(&RT&zr&Ri&rv&Ri&ra&z)&P- &RNot Active&D\n\r", ch );
                        return;
                }
                else if ( trivia->active )
                {
                        sprintf( buf, "&P-&z(&RT&zr&Ri&rv&Ri&ra&z)&P-&B Question: %s&B for&R %d&B Trivia Points&D\n\r", trivia->question, trivia->points );
                        send_to_char( buf, ch );
                        send_to_char( "&P-&z(&RT&zr&Ri&rv&Ri&ra&z)&P- &BIf you would like to ANSWER type&z (&Rtrivia answer <answer>&z)\n\r", ch );
                        return;
                }
        }
        else if ( !str_prefix(arg, "spend") )
        {
                if ( !argument[0] )
                {
                        send_to_char( "&RYou can currently get the following rewards:\n\r", ch );
                     send_to_char( "&C-&c=&C-&c=&C-&c=&RN&za&Rm&ze&C-&c=&C-&c=&C-&c=&C-&c=&C-&c=&RC&zo&Rs&zt&C-&c=&C-&c=&C-&c=&C-&c=&C-&c=&C-&c=&C-&c=&C-&c=&C-&c=&C-&c=&C-&c=&D\n\r", ch );
                     send_to_char( " &BQuest Token&z       &z        25&D\n\r", ch );
                     send_to_char( " &BSenzu Bean&z       &z        50&D\n\r", ch );
                     send_to_char( " &BMermaid Earrings&z &z        75&D\n\r", ch );
                     send_to_char( " &B1 Mill Zeni Check  &z       100&D\n\r", ch );
                     send_to_char( " &BPotara Left        &z       175&D\n\r", ch );
                     send_to_char( " &BPotara Right       &z       175&D\n\r", ch );
                     send_to_char( " &BOrb Of Memories     &z      200&D\n\r", ch );
                     send_to_char( " &BFlying Nimbus       &z      250&D\n\r", ch );
                     send_to_char( " &BBio-synth&z                 300&D\n\r", ch );
                     send_to_char( " &BOmega 2nd class Chip&z      350&D\n\r", ch );
                     send_to_char( " &BRing of Time&z              500&D\n\r", ch );
                     send_to_char( " &Bsentient Chip&z             500&D\n\r", ch );


                    return;
                }
                if ( !str_prefix(argument, "senzu bean" ) )
                {
                        if ( ch->pcdata->tpoints < 50 )
                        {
                                send_to_char( "You do not have 50 Trivia Points!\n\r", ch );
                                return;
                        }
                        if ( ( pObjIndex = get_obj_index( 2001 ) ) == NULL )
                        {
                                send_to_char( "!BUG! Inform eighteen !BUG!\n\r", ch );
                                return;
                        }
                        obj = create_object_new( pObjIndex, 20, ORIGIN_OINVOKE, ch->name );
                        sprintf( buf, "You have won a %s for being so smart!\r\n", obj->short_descr );
                        send_to_char( buf, ch );
                        obj_to_char( obj, ch );
                        ch->pcdata->tpoints -= 50;
                        do_save(ch, "" );
                }
                if ( !str_prefix(argument, "Mermaid Earrings" ) )
                {
                        if ( ch->pcdata->tpoints < 75 )
                        {
                                send_to_char( "You do not have 75 Trivia Points!\n\r", ch );
                                return;
                        }
                        if ( ( pObjIndex = get_obj_index( 2013 ) ) == NULL )
                        {
                                send_to_char( "!BUG! Inform eighteen !BUG!\n\r", ch );
                                return;
                        }
                        obj = create_object_new( pObjIndex, 20, ORIGIN_OINVOKE, ch->name );
                        sprintf( buf, "You have won a %s for being so smart!\r\n", obj->short_descr );
                        send_to_char( buf, ch );
                        obj_to_char( obj, ch );
                        ch->pcdata->tpoints -= 75;
                        do_save(ch, "" );
                }
                if ( !str_prefix(argument, "potara left" ) )
                {
                        if ( ch->pcdata->tpoints < 150 )
                        {

                                send_to_char( "You do not have 150 Trivia Points!\n\r", ch );
                                return;
                        }
                        if ( ( pObjIndex = get_obj_index( 77 ) ) == NULL )
                        {
                                send_to_char( "!BUG! Inform eighteen !BUG!\n\r", ch );
                                return;
                        }
                        obj = create_object_new( pObjIndex, 20, ORIGIN_OINVOKE, ch->name );
                        sprintf( buf, "You have won a %s for being so smart!\r\n", obj->short_descr );
                        send_to_char( buf, ch );
                        obj_to_char( obj, ch );
                        ch->pcdata->tpoints -= 150;
                        do_save(ch, "" );
                }
                if ( !str_prefix(argument, "potara right" ) )
                {
                        if ( ch->pcdata->tpoints < 150 )
                        {
                                send_to_char( "You do not have 150 Trivia Points!\n\r", ch );
                                return;
                        }
                        if ( ( pObjIndex = get_obj_index( 76 ) ) == NULL )
                        {
                                send_to_char( "!BUG! Inform eighteen !BUG!\n\r", ch );
                                return;
                        }
                        obj = create_object_new( pObjIndex, 20, ORIGIN_OINVOKE, ch->name );
                        sprintf( buf, "You have won a %s for being so smart!\r\n", obj->short_descr );
                        send_to_char( buf, ch );
                        obj_to_char( obj, ch );
                        ch->pcdata->tpoints -= 150;
                        do_save(ch, "" );
                }
                  if ( !str_prefix(argument, "quest token" ) )
                {
                        if ( ch->pcdata->tpoints < 25 )
                        {
                                send_to_char( "You do not have 25 Trivia Points!\n\r", ch );
                                return;
                        }
                        if ( ( pObjIndex = get_obj_index( 613 ) ) == NULL )
                        {
                                send_to_char( "!BUG! Inform eighteen !BUG!\n\r", ch );
                                return;
                        }
                        obj = create_object_new( pObjIndex, 20, ORIGIN_OINVOKE, ch->name );
                        sprintf( buf, "You have won a %s for being so smart!\r\n", obj->short_descr );
                        send_to_char( buf, ch );
                        obj_to_char( obj, ch );
                        ch->pcdata->tpoints -= 25;
                        do_save(ch, "" );
                }
                if ( !str_prefix(argument, "Omega 2nd class chip" ) )
                {
                        if ( ch->pcdata->tpoints < 350 )
                        {
                                send_to_char( "You do not have 350 Trivia Points!\n\r", ch );
                                return;
                        }
                        if ( ( pObjIndex = get_obj_index( 2315 ) ) == NULL )
                        {
                                send_to_char( "!BUG! Inform eighteen!BUG!\n\r", ch );
                                return;
                        }
                        obj = create_object_new( pObjIndex, 20, ORIGIN_OINVOKE, ch->name );
                        sprintf( buf, "You have won a %s for being so smart!\r\n", obj->short_descr );
                        send_to_char( buf, ch );
                        obj_to_char( obj, ch );
                        ch->pcdata->tpoints -= 350;
                        do_save(ch, "" );
                }
                  if ( !str_prefix(argument, "sentient chip" ) )
                {
                        if ( ch->pcdata->tpoints < 500 )
                        {
                                send_to_char( "You do not have 500 Trivia Points!\n\r", ch );
                                return;
                        }
                        if ( ( pObjIndex = get_obj_index( 1337 ) ) == NULL )
                        {
                                send_to_char( "!BUG! Inform eighteen !BUG!\n\r", ch );
                                return;
                        }
                        obj = create_object_new( pObjIndex, 20, ORIGIN_OINVOKE, ch->name );
                        sprintf( buf, "You have won a %s for being so smart!\r\n", obj->short_descr );
                        send_to_char( buf, ch );
                        obj_to_char( obj, ch );
                        ch->pcdata->tpoints -= 500;
                        do_save(ch, "" );
                }
                 if ( !str_prefix(argument, "1 mill zeni check" ) )
                {
                        if ( ch->pcdata->tpoints < 100 )
                        {
                                send_to_char( "You do not have 100 Trivia Points!\n\r", ch );
                                return;
                        }
                        if ( ( pObjIndex = get_obj_index( 1353 ) ) == NULL )
                        {
                                send_to_char( "!BUG! Inform eighteen !BUG!\n\r", ch );
                                return;
                        }
                        obj = create_object_new( pObjIndex, 20, ORIGIN_OINVOKE, ch->name );
                        sprintf( buf, "You have won a %s for being so smart!\r\n", obj->short_descr );
                        send_to_char( buf, ch );
                        obj_to_char( obj, ch );
                        ch->pcdata->tpoints -= 100;
                        do_save(ch, "" );
                }

                  if ( !str_prefix(argument, "Orb Of Memories" ) )
                {
                        if ( ch->pcdata->tpoints < 200 )
                        {
                                send_to_char( "You do not have 200 Trivia Points!\n\r", ch );
                                return;
                        }
                        if ( ( pObjIndex = get_obj_index( 212410 ) ) == NULL )
                        {
                                send_to_char( "!BUG! Inform eighteen !BUG!\n\r", ch );
                                return;
                        }
                        obj = create_object_new( pObjIndex, 20, ORIGIN_OINVOKE, ch->name );
                        sprintf( buf, "You have won a %s for being so smart!\r\n", obj->short_descr );
                        send_to_char( buf, ch );
                        obj_to_char( obj, ch );
                        ch->pcdata->tpoints -= 200;
                        do_save(ch, "" );
             }
                  if ( !str_prefix(argument, "flying nimbus" ) )
                {
                        if ( ch->pcdata->tpoints < 250 )
                        {
                                send_to_char( "You do not have 250 Trivia Points!\n\r", ch );
                                return;
                        }
                        if ( ( pObjIndex = get_obj_index( 60204 ) ) == NULL )
                        {
                                send_to_char( "!BUG! Inform eighteen !BUG!\n\r", ch );
                                return;
                        }
                        obj = create_object_new( pObjIndex, 20, ORIGIN_OINVOKE, ch->name );
                        sprintf( buf, "You have won a %s for being so smart!\r\n", obj->short_descr );
                        send_to_char( buf, ch );
                        obj_to_char( obj, ch );
                        ch->pcdata->tpoints -= 250;
                        do_save(ch, "" );
                }
                 if ( !str_prefix(argument, "Ring of time" ) )
                {
                        if ( ch->pcdata->tpoints < 500 )
                        {
                                send_to_char( "You do not have 500 Trivia Points!\n\r", ch );
                                return;
                        }
                        if ( ( pObjIndex = get_obj_index( 210321 ) ) == NULL )
                        {
                                send_to_char( "!BUG! Inform eighteen !BUG!\n\r", ch );
                                return;
                        }
                        obj = create_object_new( pObjIndex, 20, ORIGIN_OINVOKE, ch->name );
                        sprintf( buf, "You have won a %s for being so smart!\r\n", obj->short_descr );
                        send_to_char( buf, ch );
                        obj_to_char( obj, ch );
                        ch->pcdata->tpoints -= 500;
                        do_save(ch, "" );
                }
                  if ( !str_prefix(argument, "bio-synth" ) )
                {
                        if ( ch->pcdata->tpoints < 300 )
                        {
                                send_to_char( "You do not have 300 Trivia Points!\n\r", ch );
                                return;
                        }
                        if ( ( pObjIndex = get_obj_index( 604 ) ) == NULL )
                        {
                                send_to_char( "!BUG! Inform eighteen !BUG!\n\r", ch );
                                return;
                        }
                        obj = create_object_new( pObjIndex, 20, ORIGIN_OINVOKE, ch->name );
                        sprintf( buf, "You have won a %s for being so smart!\r\n", obj->short_descr );
                        send_to_char( buf, ch );
                        obj_to_char( obj, ch );
                        ch->pcdata->tpoints -= 300;
                        do_save(ch, "" );
                }

        }
        else if ( !str_prefix(arg, "makequestion") )
        {
                if ( !IS_IMMORTAL(ch) )
                        return;
                if ( !trivia->active )
                {
                        trivia->question = STRALLOC( argument );
                        sprintf( buf, "&BYou set the Trivia Question to&R %s.\n\r", argument );
                        send_to_char( buf, ch );
                        return;
                }
                else if ( trivia->active )
                {
                        send_to_char( "Trivia already active.\n\r", ch );
                        return;
                }
        }
        else if ( !str_prefix(arg, "makeanswer" ) )
        {
                if ( !IS_IMMORTAL(ch) )
                        return;
                if ( !str_cmp(trivia->question, "none") )
                {
                        send_to_char( "Please set a trivia question with \"trivia makequestion <question>\" before making an answer.\n\r", ch );
                        return;
                }
                else if ( !trivia->active )
                {
                        trivia->answer = STRALLOC( argument );
                        sprintf( buf, "&BYou set the Trivia Answer to&R %s.\n\r", argument );
                        send_to_char( buf, ch );
                        return;
                }
                else if ( trivia->active )
                {
                        send_to_char( "Trivia already active.\n\r", ch );
                        return;
                }
        }
        else if ( !str_prefix(arg, "points" ) )
        {
                if ( !IS_IMMORTAL(ch) )
                        return;
                if ( !trivia->active )
                {
                        if ( atoi(argument) > 10000 )
                                return;
                        trivia->points = atoi(argument);
                        sprintf( buf, "You set the Trivia Point Value to %d.\n\r", atoi(argument) );
                        send_to_char( buf, ch );
                        return;
                }
                else if ( trivia->active )
                {
                        send_to_char( "Trivia already active.\n\r", ch );
                        return;
                }
        }
        else if ( !str_prefix(arg, "done" ) )
        {
                if ( !IS_IMMORTAL(ch) && ch->level < 60 )
                        return;
                if ( !str_cmp(trivia->question, "none") )
                {
                        send_to_char( "Please set a trivia question with \"trivia makequestion <question>\" before finishing.\n\r", ch );
                        return;
                }
                else if ( !str_cmp(trivia->answer, "none") )
                {
                        send_to_char( "Please set a trivia answer with \"trivia makeanswer <answer>\" before finishing.\n\r", ch );
                        return;
                }
                else if ( trivia->points < 0 )
                {
                        send_to_char( "Please set a point value with \"trivia points <points>\" before finishing.\n\r", ch );
                        return;
                }
                else if ( !trivia->active )
                {
                        sprintf( buf, "&P*&z(&RT&zr&Ri&zv&Ri&za&z)&P* &BA new Trivia has been commenced by &R%s&w!\n\r", ch->name );
                        echo_to_all(AT_BLUE, buf, ECHOTAR_ALL );
                        sprintf( buf, "&P*&z(&RT&zr&Ri&zv&Ri&za&z)&P* &BThe Question for this Trivia is set at &R\"%s\"&B with a Point Value of &R\"%d\"&w!\n\r", trivia->question, trivia->points );
                        echo_to_all(AT_BLUE, buf, ECHOTAR_ALL );
                        sprintf( buf, "&P*&z(&RT&zr&Ri&zv&Ri&za&z)&P* &BYou have &R5 minutes&B left to answer this question.\n\r" );
                        echo_to_all(AT_BLUE, buf, ECHOTAR_ALL );
                        sprintf( buf, "&P*&z(&RT&zr&Ri&zv&Ri&za&z)&P* &BTo answer this Trivia Session, type &z(&Rtrivia answer <answer>&z)&w!\n\r" );
                        echo_to_all(AT_BLUE, buf, ECHOTAR_ALL );

                        trivia->active = TRUE;
                        trivia->time_left = 5;
                        return;
                }
                else if ( trivia->active )
                {
                        send_to_char( "&BTrivia already active.\n\r", ch );
                        return;
                }
        }
        else if ( !str_prefix( arg, "answer" ) )
        {
                if ( !trivia->active )
                {
                        send_to_char( "&BNo trivia going on.\n\r", ch );
                        return;
                }
                else if ( trivia->active )
                {
                        if ( str_cmp(argument, trivia->answer) )
                        {
                                send_to_char( "&BSorry, that answer is incorrect.\n\r", ch );
                                return;
                        }
                        else if ( !str_cmp(argument, trivia->answer) )
                        {
                                send_to_char( "&RCongratulations. That answer is correct.\n\r", ch );
                                sprintf( buf, "&P*&z(&RT&zr&Ri&zv&Ri&za&z)&P* &R%s &Bhas just answered the question correctly!&D\n\r", ch->name );
                                echo_to_all(AT_BLUE, buf, ECHOTAR_ALL );
                                sprintf( buf, "&P*&z(&RT&zr&Ri&zv&Ri&za&z)&P* &BThe correct answer was&R %s&D\n\r", trivia->answer );
                                echo_to_all(AT_BLUE, buf, ECHOTAR_ALL );
                                ch->pcdata->tpoints += trivia->points;
                                sprintf( buf, " &BYou have earned&R %d &BTrivia Points for answering right.\n\r", trivia->points );
                                send_to_char( buf, ch );
                                trivia->time_left = 5;
                                trivia->active = FALSE;
                                trivia->question = STRALLOC( "none" );
                            trivia->answer = STRALLOC( "none" );
                                trivia->points = -1;
                                                return;
                        }
                }
}
}



int get_zeni_income( CHAR_DATA *ch )
{
        long double zeni = 0;

        if ( ch->zeni_base < 1000 )
                ch->zeni_base = 1000;

        if ( ch->zeni_mks < 1 )
                ch->zeni_mks = 1;

        if ( ch->zeni_pks < 1 )
                ch->zeni_pks = 1;

        if ( ch->zeni_mds < 1 )
                ch->zeni_mds = 1;

        if ( ch->zeni_pds < 1 )
                ch->zeni_pds = 1;

        //zeni = ch->zeni_base * ch->zeni_mks * (ch->zeni_pks*2) / ch->zeni_mds / (ch->zeni_pds*2) * (100*ch->level$
        //zeni = ch->zeni_base * ch->zeni_mks * ch->zeni_pks * (ch->level*100) / ch->zeni_mds / ch->zeni_pds;
//      zeni += ch->zeni_base * ch->zeni_mks * ch->zeni_pks / ch->zeni_mds / ch->zeni_pds * (ch->level+100);
        zeni = ch->zeni_base * (ch->zeni_mks + ch->zeni_pks -  ch->zeni_mds - ch->zeni_pds + (ch->level*1.2));

        if ( zeni > 3000000 )
                zeni = 3000000;

        return (zeni);
}


void do_lset( CHAR_DATA *ch, char *argument )
{
        char buf[MAX_STRING_LENGTH];
        char arg[MAX_INPUT_LENGTH];

        argument = one_argument(argument, arg);

        if ( !arg[0] )
        {
                sprintf( buf, "Your current Login message is: %s\n\r", ch->pcdata->loginmsg );
                send_to_char( buf, ch );
                sprintf( buf, "Your current Logout message is: %s\n\r", ch->pcdata->logoutmsg );
                send_to_char( buf, ch );
                return;
        }

        if ( !str_cmp(arg, "login" ) )
        {
                if ( !has_phrase( ch->name,argument ) && !IS_IMMORTAL(ch) )
                {
                        send_to_char( "You must include your name!\n\r", ch );
                        return;
                }
                sprintf( buf, "Your old login message was: %s\n\r", ch->pcdata->loginmsg );
                ch->pcdata->loginmsg = STRALLOC( argument );
                sprintf( buf, "Your new login message is: %s\n\r", ch->pcdata->loginmsg );
                return;
        }

        if ( !str_cmp(arg, "logout" ) )
        {
                if ( !has_phrase( ch->name,argument ) && !IS_IMMORTAL(ch) )
                {
                        send_to_char( "You must include your name!\n\r", ch );
                        return;
                }
                sprintf( buf, "Your old logout message was: %s\n\r", ch->pcdata->logoutmsg );
                ch->pcdata->logoutmsg = STRALLOC( argument );
                sprintf( buf, "Your new logout message is: %s\n\r", ch->pcdata->logoutmsg );
                return;
        }
        else
        {
                send_to_char( "That is not a valid option.\n\r", ch);
                return;
        }
        return;
}

void do_hpbuy( CHAR_DATA *ch, char *argument )
{
   int max = 300;
   int cost = 500000000;
   int cost2 = 1000000000;
   int cost3 = 1500000000;
   int cost4 = 2000000000;
   char arg[MAX_INPUT_LENGTH];
   argument = one_argument( argument, arg );

  if( !str_cmp(arg, "\0"))
  {
   if( ch->exp >= 1000000000000000ULL )
   {
   send_to_char( "&W(1)&R25hp&w = &Y500,000,000 &W(2)&R50hp&w =&Y 1,000,000,000  &W(3)&R75hp&w = &Y 1,500,000,000 &W(4)&R100hp &w= &Y2,000,000,000&D\n\r", ch);
   send_to_char( "&wSyntax: hpbuy <#>&D\n\r", ch);
   return;
   }
   else
   {
   send_to_char( "&W(1)&R25hp&w = &Y500,000,000 &W(2)&R50hp&w =&Y 1,000,000,000  &W(3)&R75hp&w = &Y 1,500,000,000 &W(4)&R100hp &w= &Y2,000,000,000&D\n\r", ch);
   send_to_char( "&wSyntax: hpbuy <#>&D\n\r", ch);
   return;
   }
  }
  if( !str_cmp(arg, "1"))
  {
   if( ch->exp >= 1000000000000000ULL )
   {
      if( ch->gold < 500000000 )
    {
     send_to_char( "Try again when you're not poor...\n\r", ch);
     return;
    }
 if( ch->max_hit >= max)
   {
    send_to_char( "Your health is already at max.\n\r", ch);
    return;
   }

   ch->gold -= 500000000;
   ch->max_hit += 25;
   send_to_char( "&RYour health increases by &C25&R points.&D\n\r", ch);
   return;
   }
   else
   {
   if( ch->gold < cost )
    {
     send_to_char( "Try again when you're not poor...\n\r", ch);
     return;
    }
   if( ch->max_hit >= max)
   {
    send_to_char( "Your health is already at max.\n\r", ch);
    return;
   }

   ch->gold -= cost;
   ch->max_hit += 25;
   send_to_char( "&RYour health increases by &C100&R points.&D\n\r", ch);
   return;
   }
 }
 if( !str_cmp(arg, "2"))
 {
   if( ch->exp >= 1000000000000000ULL )
   {
   if( ch->gold < 1000000000 )
    {
     send_to_char( "Try again when you're not poor...\n\r", ch);
     return;
    }
   if( ch->max_hit >= max )
   {
    send_to_char( "Your health is already at max.\n\r", ch);
    return;
   }

   ch->gold -= 1000000000;
   ch->max_hit += 50;
   send_to_char( "&RYour health increases by &C50&R points.&D\n\r", ch);
   return;
   }
   else
   {
   if( ch->gold < cost2 )
    {
     send_to_char( "Try again when you're not poor...\n\r", ch);
     return;
    }
   if( ch->max_hit >= max)
   {
    send_to_char( "Your health is already at max.\n\r", ch);
    return;
   }

   ch->gold -= cost2;
   ch->max_hit += 50;
   send_to_char( "&RYour health increases by &C50&R points.&D\n\r", ch);
   return;
   }

 }

  
     if( !str_cmp(arg, "3"))
  {
   if( ch->exp >= 1000000000000000ULL )
   {
   if( ch->gold < 1500000000 )
    {
     send_to_char( "Try again when you're not poor...\n\r", ch);
     return;
    }
   if( ch->max_hit >= max )
   {
    send_to_char( "Your health is already at max.\n\r", ch);
    return;
   }

   ch->gold -= 1500000000;
   ch->max_hit += 75;
   send_to_char( "&RYour health increases by &C75&R points.&D\n\r", ch);
   return;
   }
   else
   {
   if( ch->gold < cost3 )
    {
     send_to_char( "Try again when you're not poor...\n\r", ch);
     return;
    }
   if( ch->max_hit >= max)
   {
    send_to_char( "Your health is already at max.\n\r", ch);
    return;
   }

   ch->gold -= cost3;
   ch->max_hit += 75;
   send_to_char( "&RYour health increases by &C75&R points.&D\n\r", ch);
   return;
   }
 }


 if( !str_cmp(arg, "4"))
  {
   if( ch->exp >= 1000000000000000ULL )
   {
   if( ch->gold < 2000000000 )
    {
     send_to_char( "Try again when you're not poor...\n\r", ch);
     return;
    }
   if( ch->max_hit >= max)
   {
    send_to_char( "Your health is already at max.\n\r", ch);
    return;
   }

   ch->gold -= 2000000000;
   ch->max_hit += 100;
   send_to_char( "&RYour health increases by &C100&R points.&D\n\r", ch);
   return;
   }
   else
   {
   if( ch->gold < cost4 )
    {
     send_to_char( "Try again when you're not poor...\n\r", ch);
     return;
    }
   if( ch->max_hit >= max)
   {
    send_to_char( "Your health is already at max.\n\r", ch);
    return;
   }

   ch->gold -= cost4;
   ch->max_hit += 100;
   send_to_char( "&RYour health increases by &C100&R points.&D\n\r", ch);
   return;
   }
 }

return;
}


void do_kibuy( CHAR_DATA *ch, char *argument )
{
   int max = 500000;
   int cost = 2500000;
   int cost2 = 5000000;
   int cost3 = 75000000;
   int cost4 = 100000000;
   char arg[MAX_INPUT_LENGTH];
   argument = one_argument( argument, arg );

  if( !str_cmp(arg, "\0"))
  {
   send_to_char( "&W(1)&B2,500ki &w = &C25,000,000 &W(2)&B5,000&w =&C 50,000,000 &W(3)&B7,500ki&w = &C 75,000,000 &W(4)&B10,000ki &w= &C100,000,000&D\n\r", ch);
   send_to_char( "&wSyntax: kibuy <#>&D\n\r", ch);
   return;
  }
  if( !str_cmp(arg, "1"))
  {
   if( ch->gold < cost )
    {
     send_to_char( "Try again when you're not poor...\n\r", ch);
     return;
    }
   if( ch->max_mana >= max)
   {
    send_to_char( "Your Ki is already at max.\n\r", ch);
    return;
   }

   ch->gold -= cost;
   ch->max_mana += 2500;
   send_to_char( "&BYour ki increases by &C2,500&B points.&D\n\r", ch);
   return;
 }
 if( !str_cmp(arg, "2"))
  {
   if( ch->gold < cost2 )
    {
     send_to_char( "Try again when you're not poor...\n\r", ch);
     return;
    }
   if( ch->max_mana >= max)
   {
    send_to_char( "Your ki is already at max.\n\r", ch);
    return;
   }

   ch->gold -= cost2;
   ch->max_mana += 5000;
   send_to_char( "&BYour ki increases by &C5,000&B points.&D\n\r", ch);
   return;

 }

  if( !str_cmp(arg, "3"))
  {
   if( ch->gold < cost3 )
    {
     send_to_char( "Try again when you're not poor...\n\r", ch);
     return;
    }
 if( ch->max_mana >= max)
   {
    send_to_char( "Your ki is already at max.\n\r", ch);
    return;
   }

   ch->gold -= cost3;
   ch->max_mana += 7500;
   send_to_char( "&BYour ki increases by &C7,500&B points.&D\n\r", ch);
   return;
 }
  if( !str_cmp(arg, "4"))
  {
   if( ch->gold < cost4 )
    {
     send_to_char( "Try again when you're not poor...\n\r", ch);
     return;
    }
 if( ch->max_mana >= max)
   {
    send_to_char( "Your ki is already at max.\n\r", ch);
    return;
   }

   ch->gold -= cost4;
   ch->max_mana += 10000;
   send_to_char( "&BYour ki increases by &C10,000&B points.&D\n\r", ch);
   return;
 }

return;
}

void do_history( CHAR_DATA *ch, char *argument )
{
    CHAN_HISTORY_DATA *history, *history_prev;
    char arg[MAX_STRING_LENGTH];
    char arg2[MAX_STRING_LENGTH];
    char buf[MAX_STRING_LENGTH];
    int channel, count, count2, num;

    if ( !str_cmp(argument,"syntax") )
    {
        stc( "&YSyntax&w: history <channel name>\n\r", ch );
        stc( "        history <last # of entries>\n\r", ch );
        stc( "\n\rAvailable Channel Names:\n\r", ch );
        stc( "  newbie, music, ooc, clan, ask\n\r", ch );
        stc( "  wartalk, racetalk", ch );
        if ( get_trust(ch) >= LEVEL_IMMORTAL )
        {
            if ( get_trust(ch) >= 62 )
                stc( ", admin", ch );
            if ( get_trust(ch) >= 62 )
                stc( ", supervisor", ch );
            stc( ", immtalk", ch );
        }
        stc( "\n\r", ch );
        return;
    }

    argument = one_argument(argument,arg);
    argument = one_argument(argument,arg2);

    num = 25;

         if ( arg[0] == '\0' )                          channel = -1;
    else if ( !str_cmp( arg, "immtalk"          ) )     channel = CHANNEL_IMMTALK;
    else if ( !str_cmp( arg, "admin"            ) )     channel = CHANNEL_ADMIN;
  //  else if ( !str_cmp( arg, "supervisor"       ) )     channel = CHANNEL_SUPERVISOR;
    else if ( !str_cmp( arg, "newbie"           ) )     channel = CHANNEL_NEWBIE;
    else if ( !str_cmp( arg, "music"            ) )     channel = CHANNEL_MUSIC;
    else if ( !str_cmp( arg, "ask"              ) )     channel = CHANNEL_ASK;
//    else if ( !str_cmp( arg, "answer"           ) )     channel = CHANNEL_ANSWER;
//    else if ( !str_cmp( arg, "swear"            ) )     channel = CHANNEL_SWEAR;
    else if ( !str_cmp( arg, "ooc"              ) )     channel = CHANNEL_OOC;
//    else if ( !str_cmp( arg, "rp"               ) )     channel = CHANNEL_RP;
//    else if ( !str_cmp( arg, "glosocial"        ) )     channel = CHANNEL_GLOSOCIAL;
    else if ( !str_cmp( arg, "wartalk"          ) )     channel = CHANNEL_WARTALK;
    else if ( !str_cmp( arg, "racetalk"         ) )     channel = CHANNEL_RACETALK;
    else if ( !str_cmp( arg, "clan"             ) )     channel = CHANNEL_CLAN;
//    else if ( !str_cmp( arg, "ally"             ) )     channel = CHANNEL_ALLY;
    else if ( atoi(arg) > 0 )
    {
        num = atoi(arg);
        channel = -1;
    }
    else
    {
        do_history( ch, "syntax" );
        return;
    }

    if ( channel != -1 )
    {
        if ( argument[0] != '\0' && atoi(arg2) > 0 )
        {
            num = atoi(arg2);
        }
    }

    count = 1000;

    for ( history = last_history; history; history = history_prev )
    {
        history_prev = history->prev;
        if ( --count <= 0 )
        {
            UNLINK( history, first_history, last_history, next, prev );
            if ( history->player )
                DISPOSE( history->player );
            if ( history->verb )
                DISPOSE( history->verb );
            if ( history->text )
                DISPOSE( history->text );
            if ( history->clan )
                history->clan = NULL;
            DISPOSE( history );
        }
    }

    count = 0;
    count2 = 0;

    for ( history = first_history; history; history = history->next )
    {
        if ( history->channel == channel || channel == -1 )
        {
            if ( history->channel == CHANNEL_RACETALK )
            {
                if ( history->race != ch->race )
                {
                    if ( !IS_IMMORTAL(ch) )
                        continue;

                    if ( argument[0] != '\0' && (get_npc_race(argument) == -1 || history->race != get_npc_race(argument)) )
                        continue;
                }
            }
            if ( history->channel == CHANNEL_CLAN )
            {
                if ( !ch->pcdata->clan || (history->clan != ch->pcdata->clan) )
                {
                    if ( !IS_IMMORTAL(ch) )
                        continue;

                    if ( argument[0] != '\0' && (get_clan(argument) == NULL || history->clan != get_clan(argument)) )
                        continue;
                }
            }
            if ( history->channel == CHANNEL_IMMTALK )
            {
                if ( get_trust(ch) < LEVEL_IMMORTAL )
                    continue;
            }
            if ( history->channel == CHANNEL_ADMIN )
            {
                if ( get_trust(ch) < 64 )
                    continue;
            }
/*            if ( history->channel == CHANNEL_SUPERVISOR )
            {
                if ( get_trust(ch) < 60 )
                    continue;
            }
            if ( history->channel == CHANNEL_ALLY )
            {
                if ( !ch->pcdata->clan || !history->clan || alliancestatus(ch->pcdata->clan,history->clan) != ALLIANCE_ALLIED )
                {
                    if ( !IS_IMMORTAL(ch) )
                        continue;

                    if ( !history->clan || (argument[0] != '\0' && get_clan(argument) != NULL && alliancestatus(get_clan(argument),history$
                        continue;
                }
            }
*/
            count2++;
        }
    }

    SET_BIT( ch->pcdata->flags, PCFLAG_PAGERON );

    pager_printf( ch, "&GChannel History for %s&w:\n\r", channel == -1 ? "All Channels" : strupper(arg) );

    for ( history = first_history; history; history = history->next )
    {
        if ( history->channel == channel || channel == -1 )
        {
            if ( history->channel == CHANNEL_RACETALK )
            {
                if ( history->race != ch->race )
                {
                    if ( !IS_IMMORTAL(ch) )
                        continue;

                    if ( argument[0] != '\0' && (get_npc_race(argument) == -1 || history->race != get_npc_race(argument)) )
                        continue;
                }
            }
            if ( history->channel == CHANNEL_CLAN )
            {
                if ( !ch->pcdata->clan || (history->clan != ch->pcdata->clan) )
                {
                    if ( !IS_IMMORTAL(ch) )
                        continue;

                    if ( argument[0] != '\0' && (get_clan(argument) == NULL || history->clan != get_clan(argument)) )
                        continue;
                }
            }
            if ( history->channel == CHANNEL_IMMTALK )
            {
                if ( get_trust(ch) < LEVEL_IMMORTAL )
                    continue;
            }
            if ( history->channel == CHANNEL_ADMIN )
            {
                if ( get_trust(ch) < 64 )
                    continue;
            }
 /*           if ( history->channel == CHANNEL_SUPERVISOR )
            {
                if ( get_trust(ch) < 60 )
                    continue;
            }
            if ( history->channel == CHANNEL_ALLY )
            {
                if ( !ch->pcdata->clan || !history->clan || alliancestatus(ch->pcdata->clan,history->clan) != ALLIANCE_ALLIED )
                {
                    if ( !IS_IMMORTAL(ch) )
                        continue;

                    if ( !history->clan || (argument[0] != '\0' && get_clan(argument) && alliancestatus(get_clan(argument),history->clan))$
                        continue;
                }
            }
*/
            count++;

            if ( count > (count2-num) )
            {
                sprintf( buf, "'%s'", history->text );

                if ( history->invis_level > get_trust(ch) )
                {
                    pager_printf( ch, "&w[&W%s&w] Invis %s", strupper(history->verb), act_string(buf, NULL, ch, NULL, NULL, STRING_NONE) );
                }
                else
                {
                    pager_printf( ch, "&w[&W%s&w] %s %s", strupper(history->verb), history->player, act_string(buf, NULL, ch, NULL, NULL, ch)); 
                }
            }
        }
    }

    REMOVE_BIT( ch->pcdata->flags, PCFLAG_PAGERON );

    if ( count2 == 0 )
        stc( "&wNo history for that channel.\n\r", ch );
    return;
}

/* truncate a char string if it's length exceeds a given value. */



void do_whereis( CHAR_DATA * ch, char *argument) // 5/16 kalisto
{
 char arg1[MAX_INPUT_LENGTH];
 char field[MAX_INPUT_LENGTH];
 char buf[MAX_STRING_LENGTH];
 CHAR_DATA *victim;
 set_char_color( AT_IMMORT, ch);


 argument = one_argument( argument, arg1);

 if( arg1[0] == '\0' )
 {
  send_to_char( "Syntax: whereis <player>\n\r", ch );
  return;
 }

   if( get_trust( ch ) < get_trust( victim ) && !IS_NPC( victim ) )
  {
    send_to_char( "You better not, that person is more goddly than you.\n\r", ch );
    return;
  }

  if( ( victim = get_char_world( ch, arg1 ) ) == NULL )
  {
    send_to_char( "They aren't online right now.\n\r", ch );
    return;
  }

	sprintf(field, "&-18s", victim->in_room->name );
	mud_trunc( field, 18);
	sprintf( buf + strlen(buf ), "%5d %-18s ", victim->in_room->vnum, field);
	send_to_pager (buf, ch);
   return;
}
