void load_changes args( (void) );
void save_changes args( (void) );
void delete_change args( (int num) );
#define CHANGES_FILE    SYSTEM_DIR "changes.dat" /* For projects  */

/*struct newchanges_data
{

    char *         change;
    char *         coder;
    char *         date;
    time_t         mudtime;
    int				imm;
    char *			type;
};
*/
//extern struct  newchanges_data * changes_table;


void nnum_changes args ((CHAR_DATA *ch));
void    ctc             args( (char *txt, CHAR_DATA * ch, int length) );
void     cent_to_char   args( (char *txt, CHAR_DATA * ch) );
void	stc		args( ( const char *txt, CHAR_DATA *ch ) );
DECLARE_DO_FUN(do_changes);
DECLARE_DO_FUN(do_addchange);
DECLARE_DO_FUN(do_chedit);
DECLARE_DO_FUN(do_addimmchange);
