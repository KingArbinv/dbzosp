#ifndef _PLAYER_H_
#define _PLAYER_H_

#define IS_SHINIGAMI(ch) (!str_cmp(get_race(ch), "shinigami"))
#define IS_QUINCY(ch) (!str_cmp(get_race(ch), "quincy"))
#define IS_HOLLOW(ch) (!str_cmp(get_race(ch), "hollow"))
#define IS_KAIO(ch) (!str_cmp(get_race(ch), "kaio"))
#define IS_DEMON(ch) (!str_cmp(get_race(ch), "demon"))
#define IS_CANDYMAN(ch) (!str_cmp(get_race(ch), "candyman"))
#define IS_ARCHDEMON(ch) (!str_cmp(get_race(ch), "archdemon"))
#define IS_ARCHANGEL(ch) (!str_cmp(get_race(ch), "archangel"))
#define IS_NEPHALIM(ch) (!str_cmp(get_race(ch), "nephalim"))
#define IS_SAIBAMAN(ch) (!str_cmp(get_race(ch), "saibaman"))
#define SAIYANRANKED(character)  (is_saiyan(character) && character->rank > 0)
#define IS_KAIRANKED(ch) (IS_KAIO(ch) && ch->rank >0)
#define IS_DEMONRANKED(ch) (IS_DEMON(ch) && ch->rank > 0)
#define IS_RANKED(ch) (ch->rank > 0)
#define IS_MAXXED(ch) (ch->exp > 200000000000000000000000ULL)
#define IS_MINUS_GAINS(ch) (weightedClothingPlMod( ch ) < 0)
#define IS_ICER(ch)  (!str_cmp(get_race(ch), "icer") || !str_cmp(get_race(ch), "ultra-icer"))
#define IS_BIOANDROID(ch)  (ch->race == 6)
#define IS_REPLOID(ch)  (!str_cmp(get_race(ch), "reploid"))
#define IS_SUPERANDROID(ch) (ch->race == 26)
#define IS_GUYVER(ch) (!str_cmp(get_race(ch), "guyver"))

//Players start here
#define IS_KALISTO(ch) (!str_cmp(ch->name, "Kalisto"))
#define IS_QUIET(ch) (!str_cmp(ch->name, "Silence"))
#define IS_SIN(ch) (!str_cmp(ch->name, "Vegus"))
#define IS_RACH(ch) (!str_cmp(ch->name, "Sarah"))
#define IS_VASH(ch) (!str_cmp(ch->name, "Vash"))
#define IS_LIGHT(ch) (!str_cmp(ch->name, "Light"))
#define is_dragon2(ch) (!str_cmp(ch->name, "Dragon") && !IS_NPC ( ch ) )
#define IS_FROST(ch) (!str_cmp(ch->name, "Frosty"))
#define IS_ZYNON(ch) (!str_cmp(ch->name, "Zynon"))
#define IS_FLO(ch) (!str_cmp(ch->name, "Flo"))
#define IS_SIX2(ch) (!str_cmp(ch->name, "Six") && !IS_NPC ( ch ) )
/* This is for colored race names when the races are coded in 



       else if( IS_QUINCY( ch ) )
      return ( "&CQ&Wu&Ci&Wn&Cc&Wy" );


       else if( IS_HOLLOW( ch ) )
      return ( "&BH&zo&Bl&zl&Bo&zw&D");
*/

//This is for custom shini tranz
#define IS_ICHIGO(ch) (!str_cmp(ch->name, "Kalisto"))
//placeholder

/* Special checks for me to do special things only i need to do for testing shit.. */
#define DEVELOPER(ch) !str_cmp(ch->name, "Zynon") 
#endif /* _PLAYER_H_ */
