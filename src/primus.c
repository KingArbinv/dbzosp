
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/wait.h>
#include <ctype.h>
#include <errno.h>
#include <stdio.h>
#include <time.h>
#include <string.h>
#include <fcntl.h>
#include <signal.h>
#include <stdarg.h>
#include "mud.h"
#ifdef USE_IMC
#include "imc.h"
#include "icec.h"
#include "house.h"
#endif

/* show a list of all unusedVNUMS */
/* By Primus */
void do_fvlist (CHAR_DATA *ch, char *argument)
{
  int i,j,vnum;
  ROOM_INDEX_DATA *room;
  char buffer[MAX_STRING_LENGTH*100]; /* should be plenty */
  char buf2 [100];
  char arg[MAX_INPUT_LENGTH];
  char *string;

  string = one_argument(argument,arg);
 
  if (arg[0] == '\0')
    {
      send_to_char("Syntax:\n\r",ch);
      send_to_char("  fvlist obj\n\r",ch);
      send_to_char("  fvlist mob\n\r",ch);
      send_to_char("  fvlist room\n\r",ch);
      return;
    }
  j=1;
  if (!str_cmp(arg,"obj"))
    {
      pager_printf(ch,"&WFree &C%s&W vnum listing for area &C%s&D\n\r",arg,
		     ch->in_room->area->name);
      pager_printf(ch,"&Y==============================================================================&C\n\r");
      for (i = ch->in_room->area->low_o_vnum; i <= ch->in_room->area->hi_o_vnum; i++) {
	if (get_obj_index(i) == NULL) {
	 pager_printf(ch,"%8d, ",i);
//	  if (j == COLUMNS) {
//	    send_to_char("\n\r",ch);
//	    j=0;
//	  }
//	  j++;
	}
      }
      send_to_char("&x\n\r",ch);
      return;
    }

  if (!str_cmp(arg,"mob"))
    { 
      pager_printf(ch,"&WFree &C%s &Wvnum listing for area &C%s&D\n\r",arg,
		     ch->in_room->area->name);
      pager_printf(ch,"&Y==============================================================================&C\n\r");
      for (i = ch->in_room->area->low_m_vnum; i <= ch->in_room->area->hi_m_vnum; i++) {
	if (get_mob_index(i) == NULL) {
	  pager_printf(ch,"%8d, ",i);
//	  if (j == COLUMNS) {
//	    send_to_char("\n\r",ch);
//	    j=0;
//	  }
//	  else j++;
	}
      }
      send_to_char("&x\n\r",ch);
      return;
    }
  if (!str_cmp(arg,"room"))
    { 
     pager_printf(ch,"&WFree &C%s &Wvnum listing for area &C%s&D\n\r",arg,
		     ch->in_room->area->name);
      pager_printf(ch,"&Y==============================================================================&C\n\r");
      for (i = ch->in_room->area->low_r_vnum; i <= ch->in_room->area->hi_r_vnum; i++) {
	if (get_room_index(i) == NULL) {
	  pager_printf(ch,"%8d, ",i);
//	  if (j == COLUMNS) {
//	    send_to_char("\n\r",ch);
//	    j=0;
//	  }
//	  else j++;
	}
      }
      send_to_char("&D\n\r",ch);
      return;
    }
  send_to_char("WHAT??? \n\r",ch);
  send_to_char("Syntax:\n\r",ch);
  send_to_char("  fvlist obj\n\r",ch);
  send_to_char("  fvlist mob\n\r",ch);
  send_to_char("  fvlist room\n\r",ch);
}


/* Shogar's code to hunt for exits/entrances to/from a zone, very nice */
/* Display improvements and overland support by Samson of Alsherok */
/* Added as per request by Primus. Added by Kalisto */
void do_aexit( CHAR_DATA * ch, char *argument )
{
   ROOM_INDEX_DATA *room;
   int i, vnum, lrange, trange;
   AREA_DATA *tarea, *otherarea;
   EXIT_DATA *pexit;
   bool found = FALSE;
#ifdef OVERLANDCODE
   ENTRANCE_DATA *enter;
#endif

   if( argument[0] == '\0' )
      tarea = ch->in_room->area;
   else
   {
      for( tarea = first_area; tarea; tarea = tarea->next )
         if( !str_cmp( tarea->filename, argument ) )
         {
            found = TRUE;
            break;
         }

      if( !found )
      {
         for( tarea = first_build; tarea; tarea = tarea->next )
            if( !str_cmp( tarea->filename, argument ) )
            {
               found = TRUE;
               break;
            }
      }

      if( !found )
      {
         send_to_char( "Area not found. Check 'zones' for the filename.\r\n", ch );
         return;
      }
   }

   trange = tarea->hi_r_vnum;
   lrange = tarea->low_r_vnum;

   for( vnum = lrange; vnum <= trange; vnum++ )
   {
      if( ( room = get_room_index( vnum ) ) == NULL )
         continue;

      if( xIS_SET( room->room_flags, ROOM_TELEPORT ) && ( room->tele_vnum < lrange || room->tele_vnum > trange ) )
      {
         pager_printf( ch, "From: %-20.20s Room: %5d To: Room: %5d (Teleport)\r\n", tarea->filename, vnum, room->tele_vnum );
      }

      for( i = 0; i < MAX_DIR + 1; i++ )
      {
         if( ( pexit = get_exit( room, i ) ) == NULL )
            continue;
#ifdef OVERLANDCODE
         if( IS_EXIT_FLAG( pexit, EX_OVERLAND ) )
         {
            pager_printf( ch, "To: Overland %4dX %4dY From: %20.20s Room: %5d (%s)\r\n",
                          pexit->x, pexit->y, tarea->filename, vnum, dir_name[i] );
            continue;
         }
#endif
         if( pexit->to_room->area != tarea )
         {
            pager_printf( ch, "To: %-20.20s Room: %5d From: %-20.20s Room: %5d (%s)\r\n",
                          pexit->to_room->area->filename, pexit->vnum, tarea->filename, vnum, dir_name[i] );
         }
      }
   }

   for( otherarea = first_area; otherarea; otherarea = otherarea->next )
   {
      if( tarea == otherarea )
         continue;
      trange = otherarea->hi_r_vnum;
      lrange = otherarea->low_r_vnum;
      for( vnum = lrange; vnum <= trange; vnum++ )
      {
         if( ( room = get_room_index( vnum ) ) == NULL )
            continue;

         if( xIS_SET( room->room_flags, ROOM_TELEPORT ) )
         {
            if( room->tele_vnum >= tarea->low_r_vnum && room->tele_vnum <= tarea->hi_r_vnum )
               pager_printf( ch, "From: %-20.20s Room: %5d To: %-20.20s Room: %5d (Teleport)\r\n",
                             otherarea->filename, vnum, tarea->filename, room->tele_vnum );
         }

         for( i = 0; i < MAX_DIR + 1; i++ )
         {
            if( ( pexit = get_exit( room, i ) ) == NULL )
               continue;

#ifdef OVERLANDCODE
            if( IS_EXIT_FLAG( pexit, EX_OVERLAND ) )
               continue;
#endif
            if( pexit->to_room->area == tarea )
            {
               pager_printf( ch, "From: %-20.20s Room: %5d To: %-20.20s Room: %5d (%s)\r\n",
                             otherarea->filename, vnum, pexit->to_room->area->filename, pexit->vnum, dir_name[i] );
            }
         }
      }
   }

#ifdef OVERLANDCODE
   for( enter = first_entrance; enter; enter = enter->next )
   {
      if( enter->vnum >= tarea->low_r_vnum && enter->vnum <= tarea->hi_r_vnum )
      {
         pager_printf( ch, "From: Overland %4dX %4dY To: Room: %5d\r\n", enter->herex, enter->herey, enter->vnum );
      }
   }
#endif
   return;
}
