#include <sys/types.h>
#include <ctype.h>
#include <stdio.h>
#include <stdarg.h>
#include <string.h>
#include <time.h>
#include <sys/stat.h>
#ifndef WIN32
#include <dirent.h>
#else
#define strcasecmp strcmp
#endif
#include "mud.h"
#include "changes.h"

void do_mud_stats(CHAR_DATA *ch, char *argument){
	int num_saiyan = 0;		// Saiyan
	int num_human = 0;		// Human
	int num_hb = 0;			// Halfbreed
	int num_namek = 0;		// Namek
	int num_android = 0;	// Android
	int num_icer = 0;		// Icer
	int num_bio = 0;		// Bio-Android
	int num_kaio = 0;		// Kaio
	int num_demon = 0;		// Demon
	int num_wizard = 0;		// Wizard
	int num_saiyan_n = 0;	// Saiyan-Namek
	int num_saiyan_h = 0;	// Saiyan-Human
	int num_saiyan_hb = 0;	// Saiyan-Halfbreed
	int num_saiyan_s = 0;	// Saiyan-Saiyan
	int num_human_n = 0;	// Human-Namek
	int num_human_hb = 0;	// Human-Halfbreed
	int num_human_h = 0;	// Human-Human
	int num_human_s = 0;	// Human-Saiyan
	int num_namek_h = 0;	// Namek-Human
	int num_namek_hb = 0;	// Namek-Halfbreed
	int num_namek_n = 0;	// Namek-Namek
	int num_namek_s = 0;	// Namek-Saiyan
	int num_hb_hb = 0;		// Halfbreed-Halfbreed
	int num_hb_s = 0;		// Halfbreed-Saiyan
	int num_hb_n = 0;		// Halfbreed-Namek
	int num_hb_h = 0;		// Halfbreed-Human
	int num_sa = 0;			// Super-Android
	int num_android_h = 0;	// Android-h
	int num_android_e = 0;	// Android-e
	int num_android_fm = 0;	// Android-fm
	int num_genie = 0;		// Genie
	int num_shinigami = 0;		//Shinigami
	int num_hollow = 0;		//Hollow
//	int num_mutant = 0;		// Mutant
	int num_kaio_k = 0;		// Kaio-k
//	int num_tuffle = 0;		// Tuffle
//	int num_konatsu = 0;	// Konatsu (Unused)
	int num_saiba = 0;		// Saiba
	int num_dragon = 0;
//	int num_darksaiba = 0;  // Dark-Saiba
	int num_invis_imm = 0;	// Invisible Immortals
	int num_invis_mort = 0;	// Invisible Mortals
	int num_imm = 0;		// Immortals
	int num_mort = 0;		// Mortals
	extern char str_boot_time[];
	DESCRIPTOR_DATA *d;
	char buf[2048];

	for( d = first_descriptor; d != NULL; d = d->next ){
		if(!d) continue;
		if(!d->character) continue;
		if(get_trust(d->character) > 50){
			if( can_see(ch, d->character) )
				num_imm++;
			if( xIS_SET(d->character->act, PLR_WIZINVIS) )
				num_invis_imm++;
		}else{
			num_mort++;
			if( IS_AFFECTED(d->character, AFF_INVISIBLE) || IS_AFFECTED(d->character, AFF_HIDE) )
				num_invis_mort++;
			if( !str_cmp(get_race(d->character), "saiyan") )
				num_saiyan++;
			if( !str_cmp(get_race(d->character), "human") )
				num_human++;
			if( !str_cmp(get_race(d->character), "halfbreed") )
				num_hb++;
			if( !str_cmp(get_race(d->character), "namek") )
				num_namek++;
			if( !str_cmp(get_race(d->character), "android") )
				num_android++;
			if( !str_cmp(get_race(d->character), "icer") )
				num_icer++;
			if( !str_cmp(get_race(d->character), "bio-android") )
				num_bio++;
			if( !str_cmp(get_race(d->character), "kaio") )
				num_kaio++;
			if( !str_cmp(get_race(d->character), "demon") )
				num_demon++;
			if( !str_cmp(get_race(d->character), "wizard") )
				num_wizard++;
			if( !str_cmp(get_race(d->character), "saiyan-n") )
				num_saiyan_n++;
			if( !str_cmp(get_race(d->character), "saiyan-h") )
				num_saiyan_h++;
			if( !str_cmp(get_race(d->character), "saiyan-hb") )
				num_saiyan_hb++;
			if( !str_cmp(get_race(d->character), "saiyan-s") )
				num_saiyan_s++;
			if( !str_cmp(get_race(d->character), "human-n") )
				num_human_n++;
			if( !str_cmp(get_race(d->character), "human-hb") )
				num_human_hb++;
			if( !str_cmp(get_race(d->character), "human-h") )
				num_human_h++;
			if( !str_cmp(get_race(d->character), "human-s") )
				num_human_s++;
			if( !str_cmp(get_race(d->character), "namek-h") )
				num_namek_h++;
			if( !str_cmp(get_race(d->character), "namek-hb") )
				num_namek_hb++;
			if( !str_cmp(get_race(d->character), "namek-n") )
				num_namek_n++;
			if( !str_cmp(get_race(d->character), "namek-s") )
				num_namek_s++;
			if( !str_cmp(get_race(d->character), "halfbreed-hb") )
				num_hb_hb++;
			if( !str_cmp(get_race(d->character), "halfbreed-s") )
				num_hb_s++;
			if( !str_cmp(get_race(d->character), "halfbreed-n") )
				num_hb_n++;
			if( !str_cmp(get_race(d->character), "halfbreed-h") )
				num_hb_h++;
			if( !str_cmp(get_race(d->character), "super-android") )
				num_sa++;
			if( !str_cmp(get_race(d->character), "android-h") )
				num_android_h++;
			if( !str_cmp(get_race(d->character), "android-e") )
				num_android_e++;
			if( !str_cmp(get_race(d->character), "android-fm") )
				num_android_fm++;
			if( !str_cmp(get_race(d->character), "genie") )
				num_genie++;
			if( !str_cmp(get_race(d->character), "shinigami") )
				num_shinigami++;
                        if( !str_cmp(get_race(d->character), "hollow") )
                                num_hollow++;
//			if( !str_cmp(get_race(d->character), "mutant") )
//				num_mutant++;
			if( !str_cmp(get_race(d->character), "kaio-k") )
				num_kaio_k++;
//			if( !str_cmp(get_race(d->character), "tuffle") )
//				num_tuffle++;
//			if( !str_cmp(get_race(d->character), "konatsu") )
//				num_konatsu++;
			if( !str_cmp(get_race(d->character), "saibaman") )
				num_saiba++;            
                        if( !str_cmp(get_race(d->character), "dragon") )
                                num_dragon++;
//			if( !str_cmp(get_race(d->character), "darksaiba") )
//				num_darksaiba++;            
		}    
	}
	/*
	sprintf( buf, "-====================================Online Races=================================-\n\r"
	"  Saiyan   : %-3d    Namek    : %-3d    Human      : %-3d    Halfbreed    : %-3d \n\r"
	"  Saiyan-n : %-3d    Namek-n  : %-3d    Human-n    : %-3d    Halfbreed-n  : %-3d \n\r"
	"  Saiyan-h : %-3d    Namek-h  : %-3d    Human-h    : %-3d    Halfbreed-h  : %-3d \n\r"
	"  Saiyan-hb: %-3d    Namek-hb : %-3d    Human-hb   : %-3d    Halfbreed-hb : %-3d \n\r"
	"  Saiyan-s : %-3d    Namek-s  : %-3d    Human-s    : %-3d    Halfbreed-s  : %-3d \n\r"
	"  Android-h: %-3d    Android-e: %-3d    Android-fm : %-3d    Super-Android: %-3d \n\r"
	"  Kaio     : %-3d	Genie    : %-3d	   Tuffle     : %-3d    Mutant       : %-3d \n\r"
	"  Kaio-k   : %-3d    Demon    : %-3d    Bio-Android: %-3d    Icer         : %-3d \n\r"
	"-====================================Online Races=================================-\n\r"
	*/
	sprintf( buf, "&B-&R====================================&WOnline Races&R==============================&B-&D\n\r"
		"  &YSaiyan   &C:&z %-3d    &GNamek    &C:&z %-3d    &wHuman      &C:&z %-3d    &cHalfbreed    &C:&z %-3d&D \n\r" 
		"  &YSaiyan-n &C:&z %-3d    &GNamek-n  &C:&z %-3d    &wHuman-n    &C:&z %-3d    &cHalfbreed-n  &C:&z %-3d&D \n\r" 
		"  &YSaiyan-h &C:&z %-3d    &GNamek-h  &C:&z %-3d    &wHuman-h    &C:&z %-3d    &cHalfbreed-h  &C:&z %-3d&D \n\r" 
		"  &YSaiyan-hb&C:&z %-3d    &GNamek-hb &C:&z %-3d    &wHuman-hb   &C:&z %-3d    &cHalfbreed-hb &C:&z %-3d&D \n\r"
		"  &YSaiyan-s &C:&z %-3d    &GNamek-s  &C:&z %-3d    &wHuman-s    &C:&z %-3d    &cHalfbreed-s  &C:&z %-3d&D \n\r"
		"  &WAndroid-h&C:&z %-3d    &WAndroid-e&C:&z %-3d    &WAndroid-fm &C:&z %-3d    &WSuper-Android&C:&z %-3d&D \n\r"
		"  &BKaio     &C:&z %-3d    &PGenie    &C:&z %-3d    &zHollow     &C:&z %-3d    &WIcer	       &C:&z %-3d&D\n\r"
		"  &BKaio-k   &C:&z %-3d    &RDemon    &C:&z %-3d    &WBio-Android&C:&z %-3d&D  &WDragon       &C:&z %-3d&D\n\r"
		"  &BWizard   &C:&z %-3d    &GSaibaman   &C:&z %-3d  &WShinigami  &C:&z %-3d&D		\n\r"
		"&B-&R====================================&WOnline Races&R=============================&B-&D\n\r", num_saiyan, num_namek, num_human, num_hb, 
		num_saiyan_n, num_namek_n, num_human_n, num_hb_n, 
		num_saiyan_h, num_namek_h, num_human_h, num_hb_h, 
		num_saiyan_hb, num_namek_hb, num_human_hb, num_hb_hb, 
		num_saiyan_s, num_namek_s, num_human_s, num_hb_s, 
		num_android_h, num_android_e, num_android_fm, num_sa, 
		num_kaio, num_genie, num_hollow, num_icer,
		num_kaio_k, num_demon, num_bio, num_dragon,  num_wizard, num_saiba, num_shinigami );
	send_to_char( buf, ch );
	send_to_char( "&R-&B====================================&CSystem Data&B===============================&R-&D\n\r", ch );
	sprintf( buf, " &WCurrent Uptime&C:&z %s&D\n\r", str_boot_time );
	send_to_char( buf, ch );
	send_to_char( "&R-&B====================================&CPlayer Data&B===============================&R-&D\n\r", ch );
	sprintf( buf, " &WMob-Kills since Hotboot   &C:&z %s&D\n\r", num_punct(mkills_since_hotboot) );
	send_to_char( buf, ch );
	sprintf( buf, " &WPlayer-Kills since Hotboot&C:&z %s&D\n\r", num_punct(pkills_since_hotboot) );
	send_to_char( buf, ch );
	sprintf( buf, " &WMob-Deaths since Hotboot  &C:&z %s&D\n\r", num_punct(mdeaths_since_hotboot) );
	send_to_char( buf, ch );
	sprintf( buf, " &WTotal PowerLevel Gained since Hotboot&C:&z %s&D\n\r", num_punct_ld(pl_since_hotboot) );
	send_to_char( buf, ch );
	sprintf( buf, " &WTotal Zeni gained since Hotboot&C:&z %s&D\n\r", num_punct(zeni_since_hotboot) );
	send_to_char( buf, ch );
	sprintf( buf, " &WTotal Commands typed since Hotboot&C:&z %s&D\n\r", num_punct(cmds_since_hotboot) );
	send_to_char( buf, ch );
	sprintf( buf, " &WLast PlayerDeath&C:&z %s &Rkilled by &z%s&D\n\r", kal_lpd, kal_lpk );
	send_to_char( buf, ch );
	if( IS_IMMORTAL(ch) ){
		sprintf( buf, "&R-&B===============&CImmortal Data&B===============&R-&D\n\r"
			" &WLast Command&C:&z %s&D\n\r", cmds_last_done );
		send_to_char( buf, ch );
	}
	send_to_char( "&R-&B=============================================================================&R-&D\n\r", ch );
	return;
}

